#define _CRT_SECURE_NO_WARNINGS 1

////用函数求字符串长度
//#include<stdio.h>
//
//int my_strlen(char* arr)
//{
//	int count = 0;
//	while (*arr != '\0')
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[10] = "abcd";
//	int ret = my_strlen(arr);
//	printf("%d\n", ret);
//	return 0;
//}


//
////用函数求字符串长度
//#include<stdio.h>
//
//int my_strlen(char* arr)
//{
//	if (*arr == '\0')
//		return 0;
//	else
//	{
//		return 1 + my_strlen(arr + 1);
//	}
//}
//
//int main()
//{
//	char arr[10] = "abcd";
//	int ret = my_strlen(arr);
//	printf("%d\n", ret);
//	return 0;
//}

//求n的阶乘。（不考虑溢出）
//#include<stdio.h>
//
//int Fac(int n)
//{
//	int tem = 1;
//	while (n > 0)
//	{
//		tem *= n;
//		n--;
//	}
//	return tem;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fac(n);
//	printf("%d\n", ret);
//	return 0;
//}


//#include<stdio.h>
//
//int Fac(int n)
//{
//	if (n <= 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return  n * Fac(n - 1);
//	}
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fac(n);
//	printf("%d\n", ret);
//	return 0;
//}


////求第n个斐波那契数。（不考虑溢出）
//#include<stdio.h>
//
//int Fan(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//		return Fan(n - 1) + Fan(n - 2);
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fan(n);
//	printf("%d\n", ret);
//	return 0;
//}


////求第n个斐波那契数。（不考虑溢出）
//#include<stdio.h>
//
//int Fan(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//	while (n > 2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fan(n);
//	printf("%d\n", ret);
//	return 0;
//}