#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#define PI 3.1415926
using namespace std;

class figure {
protected:
	double x, y;
public:
	void set_dim(double i, double j = 0) {
		x = i;
		y = j;
	}
	virtual void show_area() {
		cout << "No area computation defined for this class.\n";
	}
	~figure()
	{
		cout << "对象空间释放，这是一个析构函数" << endl;
	}
};
class triangle :public figure {
public:
	void show_area() {
		cout << "Triangle with height " << x << " and base " << y << " has an area of " << x * 0.5 * y << endl;
	}
};
class square :public figure {
public:
	void show_area() {
		cout << "Square with dimensions " << x << " and " << y << " has an area of " << x * y << endl;
	}
};
class circle :public figure {
public:
	void show_area() {
		cout << "Circle with radius " << x << " has an area of " << PI * x * x << endl;
	}
};

int main() {
	figure* p;
	triangle t;
	square s;
	circle c;
	p = &t;
	p->set_dim(10.0, 5.0);
	p->show_area();
	p = &s;
	p->set_dim(10.0, 5.0);
	p->show_area();
	p = &c;
	p->set_dim(10.0);
	p->show_area();
	return 0;
}
