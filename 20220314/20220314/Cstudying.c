#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//int main() {
//    int num1, num2, count = 0;
//    double average = 0, sum = 0;
//    scanf("%d", &num1);
//    if (num1 <= 0) {
//        printf("average = 0.0\n");
//        printf("count = 0");
//    }
//    for (int i = 0; i < num1; i++) {
//        scanf("%d", &num2);
//        if (num2 >= 60) {
//            count++;
//        }
//        sum += num2;
//    }
//    average = sum * 1.0 / num1;
//    printf("average = %.1lf\n", average);
//    printf("count = %d", count);
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    char ch;
//    int array[4] = { 0 };
//    scanf("%c", &ch);
//    for (int i = 0; i < 9; i++) {
//        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')) {
//            array[0]++;
//        }
//        else if (ch == ' ' || ch == '\n') {
//            array[1]++;
//        }
//        else if (ch >= '0' && ch <= '9') {
//            array[2]++;
//        }
//        else {
//            array[3]++;
//        }
//        scanf("%c", &ch);
//    }
//    printf("letter = %d, blank = %d, digit = %d, other = %d", array[0], array[1], array[2], array[3]);
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    int num = 0;
//    int flag = -1;
//    scanf("%d", &num);
//    if (num <= 2000 || num > 2100) {
//        printf("Invalid year");
//        return 0;
//    }
//    for (int i = 2001; i <= num; i++) {
//        if ((i % 4 == 0 && i % 100 != 0) || (i % 400) == 0) {
//            flag = 1;
//            printf("%d\n", i);
//        }
//    }
//    if (flag == -1) {
//        printf("None\n");
//    }
//    return 0;
//}


//#include<stdio.h>
//int main() {
//    int num, count = 0;
//    double array[4] = { 3.00, 2.50, 4.10, 10.20 };
//    printf("[1] apple\n[2] pear\n[3] orange\n[4] grape\n[0] exit\n");
//    while (scanf("%d", &num) != EOF) {
//        if (num == 0 || count >= 5) {
//            return 0;
//        }
//        if (num < 0 && num > 4) {
//            printf("price = 0.00\n");
//        }
//        else {
//            printf("price = %.2f\n", array[num - 1]);
//        }
//        count++;
//    }
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    int num1, num2, num3, temp;
//    scanf("%d %d %d", &num1, &num2, &num3);
//    if (num1 > num2) {
//        temp = num1;
//        num1 = num2;
//        num2 = temp;
//    }
//    if (num2 > num3) {
//        temp = num2;
//        num2 = num3;
//        num3 = temp;
//    }
//    printf("%d->%d->%d",num1, num2, num3);
//    return 0;
//}


//#include<stdio.h>
//int main() {
//    int num1, num2;
//    double ratio;
//    scanf("%d %d", &num1, &num2);
//    if (num1 > num2) {
//        ratio = 1.0 * (num1 - num2) / num2 * 100;
//        if (ratio < 10) {
//            printf("OK");
//        }
//        else if (ratio < 50) {
//            printf("Exceed %.0lf%%. Ticket 200");
//        }
//        else {
//            printf("Exceed %.0lf%%. Revoked");
//        }
//    }
//    else {
//        printf("OK");
//    }
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    int array[5] = { 0 };
//    int num1, num2;
//    scanf("%d", &num1);
//    for (int i = 0; i < num1; i++) {
//        scanf("%d", &num2);
//        if (num2 >= 90) {
//            array[0]++;
//        }
//        else if (num2 >= 80) {
//            array[1]++;
//        }
//        else if (num2 >= 70) {
//            array[2]++;
//        }
//        else if (num2 >= 60) {
//            array[3]++;
//        }
//        else {
//            array[4]++;
//        }
//    }
//    for (int j = 0; j < 5; j++) {
//        printf("%d ", array[j]);
//    }
//    return 0;
//}


//#include<stdio.h>
//#include<math.h>
//int main() {
//    double x1, y1, x2, y2, x3, y3, l, s;
//    double a, b, c, p;
//    scanf("%lf %lf %lf %lf %lf %lf", &x1, &y1, &x2, &y2, &x3, &y3);
//    if (x1 / y1 == x2 / y2 && x2 / y2 == x3 / y3) {
//        printf("Impossible");
//        return 0;
//    }
//    a = sqrt(pow(abs(x1 - x2), 2) + pow(abs(y1 - y2), 2));
//    b = sqrt(pow(abs(x1 - x3), 2) + pow(abs(y1 - y3), 2));
//    c = sqrt(pow(abs(x3 - x2), 2) + pow(abs(y3 - y2), 2));
//    l = a + b + c;
//    p = l / 2.0;
//    s = sqrt(p * (p - a) * (p - b) * (p - c));
//    printf("L = %.2f, A = %.2f\n", l, s);
//    return 0;
//}


#include<stdio.h>
int main() {
    int flag = 1;
    double eps, sum = 0;
    scanf("%lf", &eps);
    for (int i = 1; eps < 1.0 / (3 * i - 2); i++) {
        sum += flag * 1.0 / (3 * i - 2);
        flag = -flag;
    }
    printf("sum = %.6lf", sum);
    return 0;
}