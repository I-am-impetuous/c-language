#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

const int maxnum = 20;
enum ErrCode {
	noErr, overflow
};

class set {

	char elements[maxnum];
	int num;

public:
	set() { 
		elements[maxnum] = { 0 };
		num = 0;
	}
	bool Member(char);  //判断元素elem是否为本集合的成员
	ErrCode AddElem(char);//将元素添加到本集合
	void RmvElem(char); //将元素从本集合中删除
	void Copy(set);     //将实参所标识的集合中的所有元素复制到本集合中去
	bool Equal(set);    //判断两集合各自包涵的元素是否完全相同
	void print();       //显示本集合的所有元素
	set Intersect(set); //求本集合与第一参数所指出的集合的交，并作为返回值
	set Union(set);     //求本集合与第一参数所指出的集合的并，并作为返回值
	bool Contain(set);  //判断本集合是否包涵实参指出的集合中所有的元素

};


//判断元素elem是否为本集合的成员
bool set::Member(char elem) {
	int i;
	for (i = 0; i < num; i++) {
		if (elements[i] == elem) {
			return true;
		}
	}
	return false;
}


//将元素添加到本集合
ErrCode set::AddElem(char elem) {
	int i = 0;
	for (i = 0; i < num; i++) {
		if (elements[i] == elem) {
			return noErr;      //集合中已有elem，不需要重复加入
		}
	}
	if (num < maxnum) {
		elements[num] = elem;
		num++;
		return noErr;
	}
	else {
		return overflow;
	}
}


//将元素从本集合中删除
void set::RmvElem(char elem) {
	int i = 0;
	for (i = 0; i < num; i++) {
		if (elements[i] == elem) {
			for (; i < num; i++) {
				elements[i] = elements[i+1];
			}
			num--;
		}
	}
}


//将实参所标识的集合中的所有元素复制到本集合中去
void set::Copy(set s) {
	int i = 0;
	for (i = 0; i < s.num; i++) {
		this->elements[i] = s.elements[i];
		this->num = s.num;
	}
}


//判断两集合各自包涵的元素是否完全相同
bool set::Equal(set s) {
	int i = 0;
	if (this->num != s.num) {
		return false;
	}
	for (i = 0; i < s.num; i++) {
		if (!(s.Member(this->elements[i]))) {
			return false;
		}
	}
	return true;
}

//显示本集合的所有元素
void set::print() {
	int i = 0;
	for (i = 0; i < num; i++) {
		cout << this->elements[i] << " ";
	}
	cout << endl;
}

//求本集合与第一参数所指出的集合的交，并作为返回值
set set::Intersect(set s1) {
	int i = 0;
	int j = 0;
	set s;
	for (i = 0; i < this->num; i++) {
		for (j = 0; j < s1.num; j++) {
			if (elements[i] == s1.elements[j]) {
				s.elements[s.num++] = elements[i];
				break;
			}
		}
	}
	return s;
}

//求本集合与第一参数所指出的集合的并，并作为返回值
set set::Union(set s1) {
	int i = 0;
	set s;
	s.Copy(s1);
	for (i = 0; i < this->num; i++) {
		if (!(s.Member(this->elements[i]))) {
			s.AddElem(this->elements[i]);
		}
	}
	return s;
}


//判断本集合是否包涵实参指出的集合中所有的元素
bool set::Contain(set s) {
	int i = 0;
	for (i = 0; i < s.num; i++) {
		if (!this->Member(s.elements[i])) {
			return false;
		}
	}
	return true;
}




int main() {
	int i = 0;
	char ch[30] = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
		'O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d' };
	set s,s1,s2,s3,s4;
	ErrCode b;

	for (i = 0; i < 10; i++) {
		b = s.AddElem(ch[i]);
		b = s1.AddElem(ch[2 * i]);
		b = s2.AddElem(ch[3 * i]);
	}

	cout << "s"<<endl;
	s.print();
	cout << "s1" << endl;
	s1.print();
	cout << "s2" << endl;
	s2.print();
	//s3.Copy(s1);
	//cout << "s3" << endl;
	//s3.print();
	s3 = s.Intersect(s1);
	s4 = s.Union(s1);
	cout << "s3" << endl;
	s3.print();
	cout << "s4" << endl;
	s4.print();

	if (s3.Contain(s4)) {
		cout << "s3 contains s4" << endl;
	}
	else {
		cout << "s3 do not constains s4" << endl;
	}

	if (s4.Contain(s3)) {
		cout << "s4 contains s3" << endl;
	}
	else {
		cout << "s4 do not constains s3" << endl;
	}

	if (s3.Equal(s4)) {
		cout << "s3 = s4" << endl;
	}
	else {
		cout << "s3 != s4" << endl;
	}

	for (i = 6; i < 10; i++) {
		s.RmvElem(ch[i]);
		s1.RmvElem(ch[i]);
		s2.RmvElem(ch[i]);
	}

	cout << "删除部分元素后：" << endl;
	cout << "s"<<endl;
	s.print();
	cout << "s1" << endl;
	s1.print();
	cout << "s2" << endl;
	s2.print();

	return 0;

}
