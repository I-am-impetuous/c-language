#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

const int maxnum = 20;
enum ErrCode;

class set {

	char elements[maxnum];
	int num;

public:
	set() {
		elements[maxnum] = {};
		num = 0;
	}
	bool Member(char);  //判断元素elem是否为本集合的成员
	ErrCode AddElem(char);//将元素添加到本集合
	void RmvElem(char); //将元素从本集合中删除
	void Copy(set);     //将实参所标识的集合中的所有元素复制到本集合中去
	bool Equal(set);    //判断两集合各自包涵的元素是否完全相同
	void print();       //显示本集合的所有元素
	set Intersect(set); //求本集合与第一参数所指出的集合的交，并作为返回值
	set Union(set);     //求本集合与第一参数所指出的集合的并，并作为返回值
	bool Contain(set);  //判断本集合是否包涵实参指出的集合中所有的元素

};

