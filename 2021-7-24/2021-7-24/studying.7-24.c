#define _CRT_SECURE_NO_WARNINGS 1


//算数操作符
//#include <stdio.h>
//
//int main()
//{
//	int a = 7 / 2;
//	int b = 7 % 2;
//	float c = 7 / 2.0;
//	printf("%d\n", a);
//	printf("%d\n", b);
//	printf("%f\n", c);
//	return 0;
//}


//移位操作符
//#include<stdio.h>
//int main()
//{
//	int a = 12;
//	int b = a >> 1;
//	int c = a << 1;
//	printf("%d\n", b);
//	printf("%d\n", c);
//	return 0;
//}


//位操作符
//#include<stdio.h>
//int main()
//{
//	int a = 5;
//	int b = 3;
//	int c = 5 & 3;
//	int d = 5 | 3;
//	int e = 5 ^ 3;
//	printf("%d\n", c);
//	printf("%d\n", d);
//	printf("%d\n", e);
//	return 0;
//}


//赋值操作符
//#include<stdio.h>
//int main()
//{
//	float a = 1;
//	float b = 2.5f;
//	a += b;
//	printf("%f\n", a);
//	return 0;
//}

//单目操作符
//#include<stdio.h>
//
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	if (num == 0)
//		printf("hehe\n");
//	if (num != 0)
//		printf("haha\n");
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sizeof(arr));
//	printf("%d\n", sizeof(arr[0]));
//	printf("%d\n", sz);
//	return 0;
//}

////~           对一个数的二进制按位取反
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	printf("%d\n", ~a);
//	return 0;
//}


//--
//++
//#include<stdio.h>
//int main()
//{
//	int num1 = 10;
//	int num2 = num1++;
//	int num = 11;
//	int num3 = --num;
//	printf("%d\n", num1);
//	printf("%d\n", num2);
//	printf("%d\n", num);
//	printf("%d\n", num3);
//	return 0;
//}

//(类型)强制类型转换
//#include<stdio.h>
//int main()
//{
//	int a = (int)4.56;
//	printf("%d\n", a);
//	return 0;
//}

////条件操作符(三目操作符)。
////比较a和b的大小。
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = a > b ? a : b;
//	printf("%d\n", c);
//	return 0;
//}

////逗号表达式的应用
//#include<stdio.h>
//int main()
//{
//	int a = 1;
//	int b = 2;
//	int c = 3;
//	int d = (a = a + b, c = b - c, b = a + c);
//	printf("%d\n", d);
//	return 0;
//}

////下标引用操作符[]
//
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4 ,5 };
//	int a = arr[2];
//	printf("%d\n",a);
//	return 0;
//}



////函数调用操作符()
//#include<stdio.h>
//
//int ADD(int x, int y)
//{
//	return (x + y);
//}
//
//int main()
//{
//	int num1 = 10;
//	int num2 = 20;
//	int num3 = ADD(num1, num2);
//	printf("%d\n", num3);
//	return 0;
//}


//将unsigned int 重新命名为 uint ，两者所表达的类型相同。
//#include<stdio.h>
//
//typedef unsigned int uint;
//
//int main()
//{
//	uint age = 22;
//	return 0;
//}

//#include<stdio.h>
//void test2()
//{
//	int b = 0;
//	b++;
//	printf("%d\n", b);
//}
//void test1()
//{
//	static int a = 0;
//	a++;
//	printf("%d\n", a);
//}
//int main() 
//{
//	int i = 0;
//	while (i < 10)
//	{
//		test1();  
//		i++;
//	}
//	i = 0;
//	while (i < 10)
//	{
//		test2();
//		i++;
//	}
//	return 0;
//}


//实现一个函数，判断一个数是不是素数。
//#include<stdio.h>
//#include<math.h>
//
//int main()
//{
//	int i = 1;
//	int n = 1;
//	int num = 0;
//	for (i = 101; i < 200; i+=2)
//	{
//		num = 0;
//		for (n = 2; n < sqrt(i); n++)
//		{
//			if (i % n == 0)
//			{
//				num++;
//				break;
//			}
//		}
//		if (num == 0)
//			printf("%d ", i);
//	}
//	return 0;
//}

//利用上面实现的函数打印100到200之间的素数.
//#include<stdio.h>
//#include<math.h>
//
//int prime_number(int x)
//{
//	int i = 0;
//	for (i = 2; i < sqrt(x); i++)
//	{
//		if (x % i == 0)
//		{
//			return 0;
//		}
//	}
//	return x;
//}
//
//int main()
//{
//	int i = 0;
//	int a = 0;
//	int b = 0;
//	int num1 = 0;
//	scanf("%d%d", &a, &b);
//	for (i = a;i <= b; i++)
//	{
//		num1 = prime_number(i);
//		if(num1 != 0)
//			printf("%d ", num1);
//	}
//	return 0;
//}

//实现函数判断year是不是润年。
//#include<stdio.h>
//
//int leap_year(int x)
//{
//	if (((x % 4 == 0) && (x % 100 != 0)) || (x % 400 == 0))
//		return x;
//	else
//		return 0;
//}
//
//int main()
//{
//	int num = 0;
//	int i = 0;
//	for (i = 1000; i <= 2000; i++)
//	{
//		num = leap_year(i);
//		if(num != 0)
//			printf("%d ", num);
//	}
//	return 0;
//}


//#include<stdio.h>
//
//void swap_(int* x, int* y)
//{
//	int z = *x;
//	*x = *y;
//	*y = z;
//}
//
//int main()
//{
//	int a = 20;
//	int b = 10;
//	swap_(&a, &b);
//	printf("%d  %d",a,b);
//	return  0;
//}

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
#include<stdio.h>

void table_(int x)
{
	int i = 0;
	int n = 0;
	for (i = 1; i <= x; i++)
	{
		for (n = 1; n <= i; n++)
		{
			printf("%-2d*%-2d=%3d  ", i, n, i * n);
		}
		printf("\n");
	}
}

int main()
{
	int num1 = 0;
	scanf("%d", &num1);
	table_(num1);
	return 0;
}