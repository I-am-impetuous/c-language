#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>


int main()
{
	int n = 0;
	int tem = 0;
	int sum = 0;
	scanf("%d", &n);
	while (n / 2 != 0)
	{
		if (tem == 0)
			sum += n;
		if (tem == 1)
			sum = sum + n - 1;
		tem = n % 2;
		n = n / 2;
		n += tem;
	}
	printf("%d\n", sum + 1);
	return 0;
}
