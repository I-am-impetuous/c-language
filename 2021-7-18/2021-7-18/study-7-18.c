#define _CRT_SECURE_NO_WARNINGS 1

//把秒换成小时、分钟和秒。
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    int d = 0;
//    printf("输入秒数：\n");
//    scanf("%d", &a);
//    b = a / 3600;
//    c = a / 60 % 60;
//    d = a % 60;
//    printf("%d %d %d", b, c, d);
//}


//输入五个数，然后计算平均值。
//#include <stdio.h>
//int main()
//{
//    int num1, i;
//    float sum = 0;
//    for (i = 0;i < 5;i++)
//    {
//        scanf("%d", &num1);
//        sum += num1;
//
//    }
//    printf("%f\n", sum / 5.0);
//    return 0;
//}


//输入两个数然后计算余数和整除数。
//#include <stdio.h>
//int main()
//{
//    int a, b;
//    scanf("%d %d", &a, &b);
//    printf("%d %d\n", a / b, a % b);
//    return 0;
//}


//打印字符串和计算字符串的大小
#include<stdio.h>
int main()
{
	char arr1[] = "abcd";
	char arr2[] = { 'a','b','c','d' };
	char arr3[] = { 'a','b','c','d','\0' };
	int len1 = strlen(arr1);
	int len2 = strlen(arr2);
	int len3 = strlen(arr3);
	printf("%s\n", arr1);
	printf("%s\n", arr2);
	printf("%s\n", arr3);

	printf("%d\n", len1);
	printf("%d\n", len2);
	printf("%d\n", len3);
	return 0;
}


