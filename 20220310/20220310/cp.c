#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//int main() {
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < 4; i++) {
//        for (j = 0; j < i; j++) {
//            printf(" ");
//        }
//        for (j = 0; j < 4 - i; j++) {
//            printf("* ");
//        }
//        printf("\n");
//    }
//}



//#include <stdio.h>
//
//#define MAXSIZE 20
//
//typedef int DataType;
//typedef struct node {
//    DataType data[MAXSIZE];
//    int length;
//}SeqList, * PSeqList;
//
////函数申明区
//PSeqList Init_SeqList();//创建一个顺序表
//int Destroy_SeqList(PSeqList* PL);//销毁顺序表
//int Input_SeqList(PSeqList L, int n);//将数据输入到顺序表中
//int Output_SeqList(PSeqList PL);//输出顺序表
//int Delete_xy(PSeqList PL, DataType x, DataType y);//您要实现的删除x~y区间元素的函数
//
//int main()
//{
//    PSeqList PL;
//    int n, x, y;
//
//    PL = Init_SeqList();
//
//    scanf("%d\n", &n);
//    Input_SeqList(PL, n);
//
//    scanf("%d %d", &x, &y);
//    Delete_xy(PL, x, y);
//
//    Output_SeqList(PL);
//
//    Destroy_SeqList(&PL);
//
//    return 0;
//}
//
////创建一个顺序表，入口参数无，返回一个指向顺序表的指针，指针值为零表示分配空间失败
//PSeqList Init_SeqList()
//{
//    PSeqList PL;
//    PL = (PSeqList)malloc(sizeof(SeqList));
//    if (PL)
//        PL->length = 0;
//
//    return (PL);
//}
//
////销毁顺序表，入口参数：为要销毁的顺序表指针地址 
//int  Destroy_SeqList(PSeqList* PL)
//{
//    if (*PL)
//        free(*PL);
//    *PL = NULL;
//
//    return  1;
//}
////将输入数据保存到顺序表中 
//int Input_SeqList(PSeqList L, int n)
//{
//    int i, x;
//    for (i = 0; i < n; i++) {
//
//        scanf("%d", &x);
//        L->data[i] = x;
//    }
//    L->length = n;
//    return 1;
//}
////顺序表整体输出
//int Output_SeqList(PSeqList PL)
//{
//    int i;
//    for (i = 0; i < PL->length; i++)
//        printf("%d ", PL->data[i]);
//
//}
//
//
//int Delete_xy(PSeqList PL, DataType x, DataType y) {
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < PL->length; i++) {
//        if (PL->data[i] > x && PL->data[i] < y) {
//            j++;
//        }
//        else {
//            PL->data[i - j] = PL->data[i];
//        }
//    }
//    return 1;
//}



#define FLAG  -1
#include <stdio.h>
#include <malloc.h>
typedef int datatype;
typedef struct node
{
    datatype data;
    struct node* next;
}LNode, * LinkList;

LinkList Creat_LinkList();


void Print_LinkList(LinkList L);/*这里忽略函数实现部分  */


int main()
{
    LinkList L;


    L = Creat_LinkList();
    if (L == NULL)
    {
        printf("L=NULL,error!");
        return 0;
    }

    Print_LinkList(L);

    return 0;
}

LinkList Creat_LinkList() {
    LinkList H = (LinkList)malloc(sizeof(LNode));
    H->next == NULL;
    int num = -1;
    scanf("%d", &num);
    while (num != -1) {
        LinkList node1 = (LinkList)malloc(sizeof(LNode));
        node1->next = NULL;
        node1->data = num;

        node1->next = H->next;
        H->next = node1;

        scanf("%d", &num);
    }
    return H;
}

void Print_LinkList(LinkList L) {
    L = L->next;
    while (L) {
        printf("%d ", L->data);
        L = L->next;
    }
}