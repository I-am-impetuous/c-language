#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include"fraction.h"


int main() {
	Fraction frac1, frac2;
	frac1.input(1, 3);
	frac2.input(2, 5);
	cout << "打印输出：" << endl;
	frac1.display();
	frac2.display();
	frac1.makeCommond(frac2);
	frac2.makeCommond(frac1);
	cout << "通分过后：" << endl;
	frac1.display();
	frac2.display();
	cout << "两数相加：" << endl;
	frac1 = frac1 + (frac2);
	frac1.display();
	cout << "两数相减：" << endl;
	frac1 = frac1 - (frac2);
	frac1.display();
	cout << "两数相乘：" << endl;
	frac1 = frac1 * (frac2);
	frac1.display();
	cout << "两数相除：" << endl;
	frac1 = frac1 / (frac2);
	frac1.display();
	cout << "两数是否相等：" << endl;
	cout << frac1.equal(frac2) << endl;
	cout << "第一个数是否大于第二个数：" << endl;
	cout << frac1.greaterThan(frac2) << endl;
	cout << "第一个数是否小于第二个数：" << endl;
	cout << frac1.lessThan(frac2) << endl;
	return 0;
}