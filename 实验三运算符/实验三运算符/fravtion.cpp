#define _CRT_SECURE_NO_WARNINGS 1
#include"fraction.h"

#include<iostream>
using namespace std;

//构造函数
Fraction::Fraction() {
	this->above = 0;
	this->below = 1;
}

//输入分数
void Fraction::input(int above, int below) {
	if (below == 0) {
		cout << "分母不能为零:" << endl;
		exit(1);
	}
	this->above = above;
	this->below = below;
}

//显示分数
void Fraction::display() {
	cout << this->above << "/" << this->below << endl;
}

//最大公约数
int Fraction::divisor(int x, int y) {

	int z = x % y;

	while (z) {
		x = y;
		y = z;
		z = x % y;
	}
	return y;
}

//最小公倍数
int Fraction::multiple(int x, int y) {
	int z = (x * y) / divisor(x, y);
	return z;
}


//约分
void Fraction::reduction() {
	int a, b, temp;
	if (below < 0) {
		above = -above;
		below = -below;
	}
	a = abs(above);
	b = abs(above);
	temp = divisor(a, b);
	this->above /= temp;
	this->below /= temp;

}


//通分
void Fraction::makeCommond(Fraction& frac) {
	int mul = multiple(this->below, frac.below);
	int z = mul / this->below;
	this->above *= z;
	this->below *= z;
	z = mul / frac.below;
	frac.above *= z;
	frac.below *= z;
}



//两分数相加
Fraction Fraction::operator+(Fraction frac) {
	this->makeCommond(frac);
	this->above += frac.above;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//本分数减去实参分数
Fraction Fraction::operator-(Fraction frac) {
	this->makeCommond(frac);
	this->above -= frac.above;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//两分数相乘
Fraction Fraction::operator*(Fraction frac) {
	this->above *= frac.above;
	this->below *= frac.below;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//本分数除以实参分数
Fraction Fraction::operator/(Fraction frac) {
	frac.reciprocal();
	this->above *= frac.above;
	this->below *= frac.below;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//求倒数
void Fraction::reciprocal() {
	int tmp = this->above;
	this->above = this->below;
	this->below = tmp;
}


//等于运算
bool Fraction::equal(Fraction frac) {
	this->makeCommond(frac);
	if (this->above == frac.above) {
		return true;
	}
	return false;
}


//大于运算
bool Fraction::greaterThan(Fraction frac) {
	this->makeCommond(frac);
	if (this->above > frac.above) {
		return true;
	}
	return false;
}


//小于运算
bool Fraction::lessThan(Fraction frac) {
	this->makeCommond(frac);
	if (this->above < frac.above) {
		return true;
	}
	return false;
}