
class  Fraction {
	int above;         //分子
	int below;         //分母

public:
	Fraction();                  //构造函数
	int divisor(int, int);       //最大公约数
	int multiple(int, int);      //最小公倍数
	void reduction();            //约分
	void makeCommond(Fraction&); //通分

	Fraction operator+(Fraction);      //两分数相加
	Fraction operator-(Fraction);      //本分数减去实参分数
	Fraction operator*(Fraction);      //两分数相乘
	Fraction operator/(Fraction);      //本分数除以实参分数

	void reciprocal();       //求倒数
	bool equal(Fraction);        //等于运算
	bool greaterThan(Fraction);  //大于运算
	bool lessThan(Fraction);     //小于运算
	void display();              //显示分数
	void input(int, int);           //输入分数

};