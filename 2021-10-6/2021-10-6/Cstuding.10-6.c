#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	printf("%p\n", Add);
//	printf("%p\n", &Add);
//	return 0;
//}


//#include<stdio.h>
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	//int (*pf)(int, int) = &Add;
//	int (*pf)(int, int) = Add;
//	int ret = Add(a, b);
//	printf("%d\n", ret);
//	ret = (*pf)(20, 30);
//	printf("%d\n", ret);
//	return 0;
//}



//#include<stdio.h>
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	
//	return 0;
//}

//#include<stdio.h>
//
//void menu()
//{
//	printf("*******************************\n");
//	printf("*****   1.add     2.sub   *****\n");
//	printf("*****   3.mul     4.div   *****\n");
//	printf("*****   0.exit            *****\n");
//	printf("*******************************\n");
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		int x, y;
//		int ret = 0;
//		menu();
//		printf("请输入选项：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = Add(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 2:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			Sub(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 3:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			Mul(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 4:
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			Div(x, y);
//			printf("ret = %d\n", ret);
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}


////#include<stdio.h>
////
////void menu()
////{
////	printf("*******************************\n");
////	printf("*****   1.add     2.sub   *****\n");
////	printf("*****   3.mul     4.div   *****\n");
////	printf("*****   0.exit            *****\n");
////	printf("*******************************\n");
////}
////
////int Add(int x, int y)
////{
////	return x + y;
////}
////
////int Sub(int x, int y)
////{
////	return x - y;
////}
////
////int Mul(int x, int y)
////{
////	return x * y;
////}
////
////int Div(int x, int y)
////{
////	return x / y;
////}
////
////int main()
////{
////	int input = 0;
////	do
////	{
////		int x, y;
////		int ret = 0;
////		menu();
////		printf("请输入选项：");
////		scanf("%d", &input);
////		int (*parr[5])(int x, int y) = { 0, Add, Sub, Mul, Div };
////		if (0 == input)
////		{
////			printf("退出游戏\n");
////		}
////		else if (input >= 1 && input <= 4)
////		{
////			printf("输入操作数：");
////			scanf("%d %d", &x, &y);
////			ret = (*parr[input])(x, y);
////			printf("ret = %d\n", ret);
////		}
////		else
////		{
////			printf("输入错误\n");
////		}
////	} while (input);
////	return 0;
////}
//
//
//#include<stdio.h>
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	
//	return 0;
//}

//#include<stdio.h>
//
//void menu()
//{
//	printf("*******************************\n");
//	printf("*****   1.add     2.sub   *****\n");
//	printf("*****   3.mul     4.div   *****\n");
//	printf("*****   0.exit            *****\n");
//	printf("*******************************\n");
//}
//
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}
//
//int Div(int x, int y)
//{
//	return x / y;
//}
//
//void Calc(int (*pf)(int, int))
//{
//	int x, y;
//	int ret = 0;
//	printf("请输入两个操作数：");
//	scanf("%d %d", &x, &y);
//	ret = (*pf)(x, y);
//	printf("ret = %d\n", ret);
//}
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		int x, y;
//		int ret = 0;
//		menu();
//		printf("请输入选项：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calc(Add);
//			break;
//		case 2:
//			Calc(Sub);
//			break;
//		case 3:
//			Calc(Mul);
//			break;
//		case 4:
//			Calc(Div);
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("输入错误\n");
//		}
//	} while (input);
//	return 0;
//}

//#include<stdio.h>
// 
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int(*pf)(int, int) = &Add;
//	int(*pfarr[4])(int, int) = { 0, Add };
//	int(*(*ppfarr)[4])(int, int) = &ppfarr;
//	return 0;
//}