#define _CRT_SECURE_NO_WARNINGS 1


//把三个数按照由大到小的顺序排列
//#include<stdio.h>
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int num3 = 0;
//	int item = 0;
//	scanf("%d %d %d",&num1,&num2,&num3);
//	if (num1 <= num2)
//	{
//		item = num1;
//		num1 = num2;
//		num2 = item;
//	}
//	if (num1 <= num3)
//	{
//		item = num1;
//		num1 = num3;
//		num3 = item;
//	}	if (num2 <= num3)
//	{
//		item = num2;
//		num2 = num3;
//		num3 = item;
//	}
//	printf("%d %d %d\n",num1,num2,num3);
//	return 0;
//}


//输出0到100之间 3的倍数
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int shu = 0;
//	for (i = 1; i < 101; i++)
//	{
//		if (i % 3 == 0)
//			printf("%d ", i);
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	printf("%c\n", '\'');
//	printf("%s\n", "\"");
//	printf("%c\n", '\\');
//	printf("%c\n", '\a ');
//	printf("%c\n", '\t');
//	printf("%c\n",'\162');
//	printf("%c\n",'\x50');
//	return 0;
//}


//求1！加到10！
#/*include<stdio.h>
int main()
{
	int n = 0;
	int i = 0;
	int sum1 = 1;
	int sum2 = 0;
	for (i = 1; i <= 10; i++)
	{
		sum1 = 1;
		for (n = 1; n <= i; n++)
			sum1 *= n;

		sum2 += sum1;
	}
	printf("%d\n", sum2);	
}*/


//求字符串的大小
//#include <stdio.h>
//int main()
//{
//	int len1 = strlen("c:\code\test.c\n");
//	printf("%d\n",len1);
//	return 0;
//}  //打印的值为13

//#include <stdio.h>
//int main()
//{
//	printf("%d\n", strlen("abcdef"));
//	// \32被解析成一个转义字符
//	printf("%d\n", strlen("c:\test\328\test.c"));
//	return 0;
//}


//选择语句
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	printf("好好学习?输入1/0>:");
//	scanf("%d", &i);
//	if (1 == i)
//	{
//		printf("拿一个好offer\n");
//	}
//	else
//	{
//		printf("回家卖红薯\n");
//	}
//	return 0;
//}


//循环语句
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 20000; i++)
//	{
//		printf("%d\n", i);
//	}
//	printf("拿到一个好offer");
//	return 0;
//}
//ctrl+c\v    ctrl+x删除


//两个数相加
//#include<stdio.h>
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int sum = 0;
//	scanf("%d %d", &num1, &num2);
//	sum = num1 + num2;
//	printf("%d\n", sum);
//	return 0;
//}


//利用函数实现两个相加
//#include<stdio.h>
//int ADD(int x, int y)
//{
//	int z = 0;
//	z = x + y;
//	return z;
//}
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int sum = 0;
//	scanf("%d %d", &num1,&num2);
//	sum = ADD(num1, num2);
//	printf("%d\n", sum);
//	return 0;
//}

//int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//int arr2[10] = { 1,2,3,4,5 };
//int arr3[10] = { 1,2,3,4,5,6,7,8,9,10 };


//把数组中的数全部打印出来
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
