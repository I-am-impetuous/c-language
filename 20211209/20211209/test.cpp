#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

int main()
{
    int a, b, c, t;


    printf("输入三个数");
    scanf("%d %d %d", &a, &b, &c);

    int* p1 = &a;
    int* p2 = &b;
    int* p3 = &c;

    if (*p1 > *p2)
    {
        t = *p1;
        *p1 = *p2;
        *p2 = t;
    }
    if (*p1 > *p3)
    {
        t = *p1;
        *p1 = *p3;
        *p3 = t;
    }
    if (*p2 > *p3)
    {
        t = *p2;
        *p2 = *p3;
        *p3 = t;
    }


    printf("结果是：%d,%d,%d\n", a, b, c);

    return 0;
}