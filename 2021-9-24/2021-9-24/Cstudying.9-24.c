#define _CRT_SECURE_NO_WARNINGS 1



//struct Book
//{
//	char name[20];
//	char auther[20];
//	float price;
//	char id[20];
//}b1 = { "cyuyan", "zhangsan", 40.0f, "1001010011" }; //b1，b2为全局变量
//
//int main()
//{
//	//struct Book b1 = { "cyuyan", "zhangsan", 40.0f, "1001010011" };
//	//struct Book b2 = { "shujujiegou", "lisi", 50.0f, "1110001101" };
//	return 0;
//}

//#include<stdio.h>
//
//struct Book
//{
//	char name[20];
//	char auther[20];
//	float price;
//	char id[20];
//};
//
//int main()
//{
//	struct Book b1 = { "cyuyan", "zhangsan", 40.0f, "1001010011" };
//	struct Book b2 = { "shujujiegou", "lisi", 50.0f, "1110001101" };
//	
//	return 0;
//}

//#include<stdio.h>
//
//typedef struct Book
//{
//	char name[20];
//	char auther[20];
//	float price;
//	char id[20];
//}Book;
//
//int main()
//{
//	Book b1 = { "cyuyan", "zhangsan", 40.0f, "1001010011" };
//	Book b2 = { "shujujiegou", "lisi", 50.0f, "1110001101" };
//
//	return 0;
//}

//#include<stdio.h>
//
//struct Book
//{
//	char name[20];
//	char auther[30];
//	float price;
//	char id[20];
//};
//
//struct Point
//{
//	int x;
//	int y;
//	struct Book b1;  //结构体中嵌套一个结构体
//};
//
//int main()
//{
//	//结构体变量访问成员
//	struct Point p1 = { 10, 20, {"cyuyan", "lisi", 40.0f, "1110001011"} };
//	printf("%d %d %s %s %f %s\n", p1.x, p1.y, p1.b1.name, p1.b1.auther, p1.b1.price, p1.b1.id);
//
//	//结构体指针访问指向变量的成员
//	struct Point* pp1 = &p1;
//	printf("%d %d %s %s %f %s\n", (*pp1).x, (*pp1).y, (*pp1).b1.name, (*pp1).b1.auther, (*pp1).b1.price, (*pp1).b1.id);
//	printf("%d %d %s %s %f %s\n", pp1->x, pp1->y, pp1->b1.name, pp1->b1.auther, pp1->b1.price, pp1->b1.id );
//	return 0;
//}


//struct S
//{
//	int data[1000];
//	int num;
//};
//struct S s = { {1,2,3,4}, 1000 };
////结构体传参
//void print1(struct S s)
//{
//	printf("%d\n", s.num);
//}
////结构体地址传参
//void print2(struct S* ps)
//{
//	printf("%d\n", ps->num);
//}
//int main()
//{
//	print1(s);  //传结构体
//	print2(&s); //传地址
//	return 0;
//}