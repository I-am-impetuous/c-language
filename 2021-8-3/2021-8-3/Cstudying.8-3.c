#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//#define NUM 2000
//#define Add(x, y) ((x)+(y))
//
//int ADD(int x, int y)
//{
//	return (x + y);
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = NUM;
//	printf("%d\n", c);
//	int sum1 = ADD(a, b);
//	printf("%d\n", sum1);
//	int sum2 = Add(a, b);
//	printf("%d\n", sum2);
//	return 0;
//}


//#include<stdio.h>
//
//#define Add(x, y) ((x)+(y))
//#define ADD(x, y)  x + y
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int sum1 = 2 * Add(a, b);
//	int sum2 = 2 * ADD(a, b);
//	printf("%d\n", sum1);     //打印值为60
//	printf("%d\n", sum2);     //打印值为40
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int n = 0;
//	int ret = 1;
//	int sum = 0;
//	scanf("%d", &n);
//	for (i = 1; i <= n; i++)
//	{
//		ret *= i;
//		sum += ret;
//	}
//	printf("%d", sum);
//	return 0;
//}


//#include <stdio.h>
//
//int main()
//{
//	int i = 0;
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//0~9
//
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hehe\n");
//	}
//
//	return 0;
//}


//#include<stdio.h>
//
//void changes(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 1; i < sz/2; i = i+2)
//	{
//		if (sz % 2 == 0)
//		{
//			int tem = arr[i];
//			arr[i] = arr[sz - i - 1];
//			arr[sz - i - 1] = tem;
//		}
//		else
//		{
//			int tem = arr[i];
//			arr[i] = arr[sz - i];
//			arr[sz - i] = tem;
//		}
//	}
//}
//
//void Print(int arr1[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//}
//
//main()
//{
//	int arr1[10] = { 0,1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	//交换数组奇偶位
//	changes(arr1, sz);
//	//打印数组的内容
//	Print(arr1, sz);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int i = 0;
//    int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//    for (i = 0; i <= 12; i++)
//    {
//        arr[i] = 0;
//        printf("hello bit\n");
//    }
//    return 0;
//}
//1、局不变量是放在栈区中的，栈区的特点是：
//先存放高地址然后存放低地址，也就是堆栈
//2、在数组的内部随着数组下标的增大数组的
//地址也增大
//3、i 创建的比arr[10]早，所以i的地址是高于
//数组任意一个地址的
//4、又因为使用的是VS2019的编译器，每定义一个
//变量之间隔着两个整型的大小，所以arr[12]的
//地址就是存放 i 的地址，所以把arr[12]赋值为零
//就是i = 0；所以造成了死循环