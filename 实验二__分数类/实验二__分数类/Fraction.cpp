#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cmath>
using namespace std;


class  Fraction {
	int above;         //分子
	int below;         //分母

public:
	Fraction();                  //构造函数
	int divisor(int, int);       //最大公约数
	int multiple(int, int);      //最小公倍数
	void reduction();            //约分
	void makeCommond(Fraction&); //通分
	Fraction add(Fraction);      //两分数相加
	Fraction sub(Fraction);      //本分数减去实参分数
	Fraction mul(Fraction);      //两分数相乘
	Fraction div(Fraction);      //本分数除以实参分数
	void reciprocal();       //求倒数
	bool equal(Fraction);        //等于运算
	bool greaterThan(Fraction);  //大于运算
	bool lessThan(Fraction);     //小于运算
	void display();              //显示分数
	void input(int, int);           //输入分数

};


//构造函数
Fraction::Fraction() {
	this->above = 0;
	this->below = 1;
}

//输入分数
void Fraction::input(int above, int below) {

	if (below == 0) {
		cout << "分母不能为零:" << endl;
		exit(1);
	}

	this->above = above;
	this->below = below;
}

//显示分数
void Fraction::display() {
	cout << this->above << "/" << this->below << endl;
}

//最大公约数
int Fraction::divisor(int x, int y) {

	int z = x % y;

	while (z) {
		x = y;
		y = z;
		z = x % y;
	}
	return y;
}

//最小公倍数
int Fraction::multiple(int x, int y) {
	int z = (x * y) / divisor(x, y);
	return z;
}


//约分
void Fraction::reduction() {

	int a, b, temp;
	if (below < 0) {
		above = -above;
		below = -below;
	}
	a = abs(above);
	b = abs(above);
	temp = divisor(a, b);
	this->above /= temp;
	this->below /= temp;
}


//通分
void Fraction::makeCommond(Fraction& frac) {
	int mul = multiple(this->below, frac.below);
	int z = mul / this->below;
	this->above *= z;
	this->below *= z;
	z = mul / frac.below;
	frac.above *= z;
	frac.below *= z;
}



//两分数相加
Fraction Fraction::add(Fraction frac) {
	this->makeCommond(frac);
	this->above += frac.above;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//本分数减去实参分数
Fraction Fraction::sub(Fraction frac) {
	this->makeCommond(frac);
	this->above -= frac.above;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//两分数相乘
Fraction Fraction::mul(Fraction frac) {
	this->above *= frac.above;
	this->below *= frac.below;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//本分数除以实参分数
Fraction Fraction::div(Fraction frac) {
	frac.reciprocal();
	this->above *= frac.above;
	this->below *= frac.below;
	this->reduction();
	frac.above = this->above;
	frac.below = this->below;
	return frac;
}


//求倒数
void Fraction::reciprocal() {
	int tmp = this->above;
	this->above = this->below;
	this->below = tmp;
}


//等于运算
bool Fraction::equal(Fraction frac) {
	this->makeCommond(frac);
	if (this->above == frac.above) {
		return true;
	}
	return false;
}


//大于运算
bool Fraction::greaterThan(Fraction frac) {
	this->makeCommond(frac);
	if (this->above > frac.above) {
		return true;
	}
	return false;
}


//小于运算
bool Fraction::lessThan(Fraction frac) {
	this->makeCommond(frac);
	if (this->above < frac.above) {
		return true;
	}
	return false;
}



int main() {
	Fraction frac1, frac2;
	frac1.input(1, 3);
	frac2.input(2, 5);
	cout << "打印输出："<<endl;
	frac1.display();
	frac2.display();
	frac1.makeCommond(frac2);
	frac2.makeCommond(frac1);
	cout << "通分过后："<<endl;
	frac1.display();
	frac2.display();
	cout << "两数相加：" << endl;
	frac1 = frac1.add(frac2);
	frac1.display();
	cout << "两数相减：" << endl;
	frac1 = frac1.sub(frac2);
	frac1.display();
	cout << "两数相乘：" << endl;
	frac1 = frac1.mul(frac2);
	frac1.display();
	cout << "两数相除：" << endl;
	frac1 = frac1.div(frac2);
	frac1.display();
	cout << "两数是否相等：" << endl;
	cout << frac1.equal(frac2) << endl;
	cout << "第一个数是否大于第二个数：" << endl;
	cout << frac1.greaterThan(frac2) << endl;
	cout << "第一个数是否小于第二个数：" << endl;
	cout << frac1.lessThan(frac2) << endl;
	return 0;
}
