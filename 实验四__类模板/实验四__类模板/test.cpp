#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

const int maxnum = 20;
enum ErrCode { noErr, overflow };

template<class T> 
class set {
	T elements[maxnum];
	int num;
public:
	set() { num = 0; }
	set(set& a) {
		int i;
		num = a.num;
		for (i = 0; i < num; i++) elements[i] = a.elements[i]; //拷贝构造函数
	}
	bool Member(T);           //判断元素elem是否为本集合的成员
	ErrCode AddElem(T);        //将新元素elem加入本集合
	void RmvElem(T);          //将元素elem从该集合中删去
	void Copy(set&);   //将本集合中的所有元素拷贝到实参指出的集合中去
	bool Equal(set&);           //判断两集合各自包涵的元素是否完全相同
	void print();               //显示本集合的所有元素
	void Intersect(set&, set&);  //求本集合与第一参数所指出的集合的交，并存入第二参数所指出的集合中
	ErrCode Union(set&, set&);   //求本集合与第一参数所指出的集合的并，并存入第二参数所指出的集合中
	bool Contain(set&); //判断本集合是否包涵实参指出的集合中所有的元素
};
template<typename T>bool set<T>::Member(T elem) {
	int i;
	for (i = 0; i < num; i++)
		if (elements[i] == elem) return true;
	return false;
}
template<typename T>ErrCode set<T>::AddElem(T elem) {
	int i;
	for (i = 0; i < num; i++)
		if (elements[i] == elem) return noErr;//集合中已有elem，不重复加入
	if (num < maxnum) {
		elements[num++] = elem;
		return noErr;
	}
	else return overflow;       //数组已满
}
template<typename T>void set<T>::RmvElem(T elem) {
	int i;
	for (i = 0; i < num; i++)
		if (elements[i] == elem) {
			for (; i < num - 1; i++)  elements[i] = elements[i + 1];//当删除一个元素后，所有后继元素前移一位
			num--;
		}
}
template<typename T>void set<T>::Copy(set& s) {
	int i;
	for (i = 0; i < num; i++) s.elements[i] = elements[i];
	s.num = num;
}
template<typename T>bool set<T>::Equal(set& s) {
	int i;
	if (num != s.num) return false;       //元素个数不同
	for (i = 0; i < num; i++)
		if (s.Member(elements[i])) return false;       //有元素不同
	return true;
}
template<typename T>void set<T>::print() {
	cout << "集合的元素包括：" << endl;
	int i;
	for (i = 0; i < num; i++)  cout << elements[i] << "  ";
	cout << endl;
}
template<typename T>void set<T>::Intersect(set& s1, set& s2) {
	int i, j;
	s2.num = 0;
	for (i = 0; i < num; i++)
		for (j = 0; j < s1.num; j++)
			if (elements[i] == s1.elements[j]) {
				s2.elements[s2.num++] = elements[i];
				break;
			}
}
template<typename T>ErrCode set<T>::Union(set& s1, set& s2) {
	int i;
	s1.Copy(s2);
	for (i = 0; i < num; i++)
		if (s2.AddElem(elements[i]) == overflow) return overflow;
	return noErr;
}
template<typename T>bool set<T>::Contain(set& s) {
	int i;
	for (i = 0; i < s.num; i++)
		if (!Member(s.elements[i])) return false;
	return true;
}

int main() {
	int i;
	char ch[30] = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
		'O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d' };
	set<char> s, s1, s2, s3, s4;
	ErrCode b;
	for (i = 0; i < 10; i++) {
		b = s.AddElem(ch[i]);
		b = s1.AddElem(ch[2 * i]);
		b = s2.AddElem(ch[3 * i]);
	}
	cout << "s";
	s.print();
	cout << "s1";
	s1.print();
	cout << "s2";
	s2.print();
	set<char> s5(s);           //调用拷贝构造函数
	cout << "s5";
	s5.print();
	s.Intersect(s1, s3);
	b = s.Union(s1, s4);
	cout << "s3";
	s3.print();
	cout << "s4";
	s4.print();
	if (s3.Contain(s4)) cout << "Set s3 contains s4" << endl;
	else cout << "Set s3 do not contains s4" << endl;
	if (s4.Contain(s3)) cout << "Set s4 contains s3" << endl;
	else cout << "Set s4 do not contains s3" << endl;
	if (s3.Equal(s4)) cout << "Set s3=s4" << endl;
	else cout << "Set s3!=s4" << endl;
	for (i = 6; i < 10; i++) {
		s.RmvElem(ch[i]);
		s1.RmvElem(ch[i]);
		s2.RmvElem(ch[i]);
	}
	cout << "删除部分元素后：" << endl;
	cout << "s";
	s.print();
	cout << "s1";
	s1.print();
	cout << "s2";
	s2.print();
	return 0;
}
