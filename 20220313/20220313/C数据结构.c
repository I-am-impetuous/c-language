#define _CRT_SECURE_NO_WARNINGS 1


//#include<stdio.h>
//int main() {
//    double h = 100.0;
//    int g = 10;
//    int t = 3;
//    double height = g * t * t / 2;
//    if (height > h) {
//        height = h;
//    }
//    printf("height = %.2d", height);
//    return 0;
//}


//#include<stdio.h>
//int main() {
//    int num1, num2;
//    scanf("%d %d", &num1, &num2);
//    printf("num1 + num2 = %d\n", num1 + num2);
//    printf("num1 - num2 = %d\n", num1 - num2);
//    printf("num1 * num2 = %d\n", num1 * num2);
//    printf("num1 / num2 = %d", num1 / num2);
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    float x;
//    float sum = 0.0;
//    scanf("%f", &x);
//    if (x == 0) {
//        sum = 0.0;
//    }
//    else {
//        sum = 1.0 / x;
//    }
//    printf("f(%.1f) = %.1f", x, sum);
//}

//#include<stdio.h>
//int main() {
//    int lower, upper, fahr;
//    float celsius, sum = 0;
//    scanf("%d %d", &lower, &upper);
//    if (lower > upper || lower <= 0 || upper > 100) {
//        printf("Invalid.");
//    }
//    else {
//        printf("fahr celsius\n");
//        fahr = lower;
//        while (fahr <= upper) {
//            sum = 5.0 * (fahr - 32) / 9.0;
//            printf("%-5d%.1f\n", fahr, sum);
//            fahr += 2;
//        }
//    }
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    int n = 0;
//    double sum = 0;
//    scanf("%d", &n);
//    for (int i = 1; i <= n; i++) {
//        sum += (1.0) / i;
//    }
//    printf("sum = %.6lf", sum);
//    return 0;
//}\


//#include<stdio.h>
//int main() {
//    int lower, upper, fahr;
//    double celsius, sum = 0;
//    scanf("%d %d", &lower, &upper);
//    if (lower > upper || lower <= 0 || upper > 100) {
//        printf("Invalid.\n");
//    }
//    else {
//        printf("fahr celsius\n");
//        fahr = lower;
//        while (fahr <= upper) {
//            sum = 5.0 * (fahr - 32) / 9;
//            printf("%d%6.1lf\n", fahr, sum);
//            fahr += 2;
//        }
//    }
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    int n;
//    double sum = 0;
//    int flag = 1;
//    scanf("%d", &n);
//    for (int i = 1; i <= n; i++) {
//        sum += flag * 1.0 / (3 * i - 2);
//        flag = -flag;
//    }
//    printf("sum = %.3lf",sum);
//    return 0;
//}

//#define FLAG  -1
//#include <stdio.h>
//#include <malloc.h>
//typedef int datatype;
//typedef struct node
//{
//    datatype data;
//    struct node* next;
//}LNode, * LinkList;
//
//
//LinkList Creat_LinkList();
//void Print_LinkList(LinkList L);
//
//
//int main()
//{
//    LinkList L;
//
//
//    L = Creat_LinkList();
//    if (L == NULL)
//    {
//        printf("L=NULL,error!");
//        return 0;
//    }
//
//    Print_LinkList(L);
//
//    return 0;
//}
//
//void Print_LinkList(LinkList L)
//{
//    LinkList cur = L->next;
//    while (cur) {
//        printf("%d ", cur->data);
//        cur = cur->next;
//    }
//}
//
//LinkList Creat_LinkList()
//{
//    LinkList s;
//    LinkList L = (LNode*)malloc(sizeof(LNode));//��ͷ�������洢�ռ�
//    int x;
//    L->next = NULL;
//    scanf("%d", &x);
//    while (x != -1)
//    {
//        s = (LNode*)malloc(sizeof(LNode));//��ָ������洢�ռ�
//        s->data = x;
//        s->next = L->next;
//        L->next = s;
//        scanf("%d", &x);
//    }
//    return L;
//}

//#include<stdio.h>
//#include<math.h>
//int main() {
//    int n = -1;
//    scanf("%d", &n);
//    for (int i = 0; i <= n; i++) {
//        int sum = pow(3, i);
//        printf("pow(3,%d) = %d\n", i, sum);
//    }
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    int num, sum = 0;
//    double average;
//    for (int i = 0; i < 4; i++) {
//        scanf("%d", &num);
//        sum += num;
//    }
//    average = 1.0 * sum / 4;
//    printf("Sum = %d; Average = %.1lf", sum, average);
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    int num;
//    double sum = 0;
//    scanf("%d", &num);
//    if (num < 0) {
//        printf("\"Invalid Value!\"");
//    }
//    if (num >= 50) {
//        sum = 50 * 0.53 + ((num - 50) * 0.58);
//    }
//    else {
//        sum = num * 0.53;
//    }
//    printf("cost = %.2lf", sum);
//    return 0;
//}

//#include<stdio.h>
//int main() {
//    float sum = 0;
//    int num, flag = 1;
//    scanf("%d", &num);
//    for (int i = 1; i <= num; i++) {
//        sum = 1.0 * flag * i / (2 * i - 1);
//        flag = -flag;
//    }
//    printf("%.3f", sum);
//    return 0;
//}

#include<stdio.h>
int main() {
    int num, sign;
    scanf("%d", &num);
    if (num > 0) {
        sign = -1;
    }
    else if (num == 0) {
        sign = 0;
    }
    else {
        sign = 1;
    }
    printf("sign(%d) = %d", num, sign);
    return 0;
}