#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//#include<assert.h>
//
//char* my_strcpy(char* arr1, const char* arr2)
//{
//	char* tem = arr1;
//	assert(arr2);
//	while (*arr1++ = *arr2++)
//	{
//		;
//	}
//	return tem;
//}
//
//int main()
//{
//	char arr1[] = "xxxxxxxxx";
//	char arr2[] = "abcd";
//	my_strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}

//#include<stdio.h>
//
//size_t my_strlen(char* arr)
//{
//	int count = 0;
//	while (*arr) 
//	{
//		arr++;
//		count++;
//	}
//	return count;
//}

//#include<stdio.h>
//
//size_t my_strlen(int* s)
//{
//	char* p = s;
//	while (*p)
//	{
//		p++;
//	}
//	return p - s;
//}

//#include<stdio.h>
//
//int my_strlen(char* p)
//{
//	if (*p == '\0')
//	{
//		return 0;
//	}
//	return 1 + my_strlen(++p);
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int ret = 0;
//	ret = my_strlen(arr);
//	printf("%d\n",ret);
//	return 0;
//}


//#include<stdio.h>
//#include<assert.h>
//
//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	//1.找到dest中字符串末尾\0
//	while (*dest)
//	{
//		dest++;
//	}
//	//2.把src拷贝到dest末尾
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "abcd";
//	char arr2[] = "efi";
//	printf("%s\n", my_strcat(arr1, arr2));
//	return 0;
//}


//#include<stdio.h>
//
//int my_strcmp(char* src1, char* src2)
//{
//	while (*src1 == *src2)
//	{
//		if (*src1 == '\0')
//			return 0;
//		src1++;
//		src2++;
//	}
//	return *src1 - *src2;
//}
//
//int main()
//{
//	char arr1[] = "abc";
//	char arr2[] = "abcd";
//	int ret = my_strcmp(arr1, arr2);
//	if (ret == 0)
//	{
//		printf("相等\n");
//	}
//	else if (ret < 0)
//	{
//		printf("小于\n");
//	}
//	else
//	{
//		printf("大于\n");
//	}
//	return 0;
//}


//#include<stdio.h>
//#include<assert.h>
//
//char* my_strncpy(char* dest, const char* src, int num)
//{
//	assert(src);
//	char* tem = dest;
//	while (*dest++ = *src++)
//	{
//		num--;
//		if (num <= 0)
//		{
//			break;
//		}
//	}
//	if (*dest != '\0')
//	{
//		*dest = '\0';
//	}
//	else
//	{
//		int i = 0;
//		for (i = 0; i < num; i++)
//		{
//			if (*(tem + i) == '\0')
//			{
//				int j = i;
//				for (j = i; j < num; j++)
//				{
//					*dest = '\0';
//				}
//				break;
//			}
//		}
//	}
//	return tem;
//}
//
//int main()
//{
//	char arr1[] = "xxxxxxxx";
//	char arr2[] = "abcd";
//	char* ret = my_strncpy(arr1, arr2, 6);
//	printf("%s\n", ret);
//	return 0;
//}


//#include<stdio.h>
//
//char* my_strncat(char* dest, char* src, int count)
//{
//	while (*dest)
//	{
//		dest++;
//	}
//	while (*dest++ = *src++)
//	{
//		count--;
//		if (count < 0)
//		{
//			break;
//		}
//	}
//}
//
//int main()
//{
//	char arr1[20] = "abcd";
//	char arr2[] = "efg";
//	char* ret = my_strncat(arr1, arr2, 3);
//	printf("%s\n", arr1);
//	return 0;
//}


#include<stdio.h>

int my_strncmp(char* str1, char* str2, int count)
{
	while (*str1 == *str2)
	{
		if (str1 == '\0')
		{
			return 0;
		}
		count--;
		str1++;
		str2++;
		if (count <= 0)
		{
			return 0;
		}
	}
	return *str1 - *str2;
}

int main()
{
	char arr1[] = "abcd";
	char arr2[] = "abcde";
	int ret = my_strncmp(arr1, arr2, 4);
	if (ret == 0)
	{
		printf("等于");
	}
	else if (ret < 0)
	{
		printf("小于");
	}
	else
	{
		printf("大于");
	}
	return 0;
}