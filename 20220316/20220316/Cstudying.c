#define _CRT_SECURE_NO_WARNINGS 1
//
//#include <stdio.h>
//#include <stdlib.h>
//typedef int Datatype;
//typedef struct node
//{
//    Datatype data;
//    struct node* next;
//}LNode, * LinkList;
//LinkList  createlist();
//LinkList  deleteodd(LinkList H);
//void printlist(LinkList H)
//{
//    LinkList p = H->next;
//    while (p) {
//        printf("%d ", p->data);
//        p = p->next;
//    }
//    printf("\n");
//}
//int main()
//{
//    LinkList H;
//    H = createlist();
//    H = deleteodd(H);
//    printlist(H);
//    return 0;
//}
//LinkList  createlist()
//{
//    int x, size;
//    LinkList p, r, h;
//    size = sizeof(LNode);
//    h = (LinkList)malloc(size);
//    r = h;
//    h->next = NULL;
//    scanf("%d", &x);
//    while (x != -1) {
//        p = (LinkList)malloc(size);
//        p->data = x;
//        p->next = NULL;
//        r->next = p;
//        r = p;
//        scanf("%d", &x);
//    }
//    return h;
//}
//
///* 请完善下列函数 */
//
//LinkList deleteodd(LinkList head) {
//
//    LinkList cur = head;
//
//    while (cur && cur->next) {
//
//        if (cur->next->data % 2 == 1) {
//
//            LinkList curNext = cur->next;
//
//            cur->next = cur->next->next;
//
//            free(curNext);
//
//        }
//        else {
//
//            cur = cur->next;
//
//        }
//
//    }
//
//    return head;
//
//}

//#include <stdio.h>
//#include <stdlib.h>
//
//#define MAXSIZE 100
//typedef char DataType;
//typedef struct Node {
//    DataType      data[MAXSIZE];
//    int top;
//    int maxS;//堆栈的最大容量
//} SeqStack, * PSeqStack;
//
//PSeqStack Init_SeqStack(int m);
//int Empty_SeqStack(PSeqStack S);
//int Push_SeqStack(PSeqStack S, DataType x);
//int Pop_SeqStack(PSeqStack S, DataType* x);
//int GetTop_SeqStack(PSeqStack S, DataType* x);
//int BracketMatch(PSeqStack S, char* str);
//
//int main()
//{
//    int M;
//    char str[MAXSIZE];
//    scanf("%d\n", &M);
//    PSeqStack S = Init_SeqStack(M);
//    scanf("%s", str);
//    if (BracketMatch(S, str)) {
//        printf("YES\n");
//    }
//    else {
//        printf("NO\n");
//    }
//    return 0;
//}
//PSeqStack Init_SeqStack(int m)
//{
//    PSeqStack S;
//    S = (PSeqStack)malloc(sizeof(SeqStack));
//    if (S) {
//        S->top = -1;
//        S->maxS = m;
//    }
//    return S;
//
//}
//
//int Empty_SeqStack(PSeqStack S)
//{
//    if (S->top == -1)
//        return 1;
//    else
//        return 0;
//}
//
//int Push_SeqStack(PSeqStack S, DataType x)
//{
//
//    if (S->top == S->maxS - 1)
//        return 0;
//    else
//    {
//        S->top++;
//        S->data[S->top] = x;
//        return 1;
//    }
//}
//int Pop_SeqStack(PSeqStack S, DataType* x)
//{
//
//    if (Empty_SeqStack(S)) {
//        return 0;
//    }
//    else
//    {
//        *x = S->data[S->top];
//        S->top--;
//        return 1;
//    }
//}
//int GetTop_SeqStack(PSeqStack S, DataType* x)
//{
//    if (Empty_SeqStack(S)) {
//        return 0;
//    }
//    else
//    {
//        *x = S->data[S->top];
//        return 1;
//    }
//}
//
//int BracketMatch(PSeqStack S, char* str) {
//    char ch = '\0';
//    while (*str != '\0') {
//        if (*str == 'S') {
//            Push_SeqStack(S, *str);
//        }
//        else {
//            if (Empty_SeqStack( S)) {
//                return 0;
//            }
//            Pop_SeqStack(S, &ch);
//        }
//        str++;
//    }
//    if (Empty_SeqStack( S)) {
//        return 1;
//    }
//    else {
//        return 0;
//    }
//
//}

//#include <stdio.h>
//#include <stdlib.h>
//
//typedef char DataType;
//typedef struct Node {
//    DataType      data;
//    struct Node* next;
//} StackNode, * PStackNode;
//
//typedef struct {
//    PStackNode top;
//} LinkStack, * PLinkStack;
//
//PLinkStack Init_LinkStack();
//int Empty_LinkStack(PLinkStack S);
//int Push_LinkStack(PLinkStack S, DataType x);
//int Pop_LinkStack(PLinkStack S, DataType* x);
//int GetTop_LinkStack(PLinkStack S, DataType* x);
//void BracketMatch(PLinkStack S);
//
//int main()
//{
//    PLinkStack S = Init_LinkStack();
//    BracketMatch(S);
//    return 0;
//}
//PLinkStack Init_LinkStack()
//{
//    PLinkStack S;
//    S = (PLinkStack)malloc(sizeof(LinkStack));
//    if (S)  S->top = NULL;
//    return S;
//}
//
//int Empty_LinkStack(PLinkStack S)
//{
//    if (S->top == NULL)
//        return 1;
//    else
//        return 0;
//}
//
//int Push_LinkStack(PLinkStack S, DataType x)
//{
//    PStackNode p;
//    p = (PStackNode)malloc(sizeof(StackNode));
//    if (p == NULL)
//        return 0;
//    else
//    {
//        p->data = x;
//        p->next = S->top;
//        S->top = p;
//        return 1;
//    }
//}
//int Pop_LinkStack(PLinkStack S, DataType* x)
//{
//    PStackNode p;
//    if (Empty_LinkStack(S)) {
//        return 0;
//    }
//    else
//    {
//        *x = S->top->data;
//        p = S->top;
//        S->top = p->next;
//        free(p);
//        return 1;
//    }
//}
//int GetTop_LinkStack(PLinkStack S, DataType* x)
//{
//    if (Empty_LinkStack(S)) {
//        return 0;
//    }
//    else
//    {
//        *x = S->top->data;
//        return 1;
//    }
//}
//
//void BracketMatch(PLinkStack S) {
//    char ch1, ch2 = '\0';
//    scanf("%c", &ch1);
//    while (ch1 != '#') {
//        if (ch1 == '(' || ch1 == '{' || ch1 == '[') {
//            Push_LinkStack(S, ch1);
//        }
//        else {
//            if (Empty_LinkStack(S)) {
//                printf("no!");
//                return;
//            }
//            else {
//                GetTop_LinkStack(S, &ch2);
//                if ((ch1 == ')' && ch2 == '(') || (ch1 == '}' && ch2 == '{') || (ch1 == ']' && ch2 == '[')) {
//                    Pop_LinkStack(S, &ch2);
//                }
//                else {
//                    printf("no!");
//                    return;
//                }
//            }
//        }
//        scanf("%c", &ch1);
//    }
//    printf("yes!");
//}


#include <stdio.h>
#include <stdlib.h>

#define ERROR -1
typedef int ElementType;
typedef enum { push, pop, end } Operation;
typedef enum { false, true } bool;
typedef int Position;
typedef struct SNode* PtrToSNode;
struct SNode {
    ElementType* Data;  /* 存储元素的数组 */
    Position Top;       /* 栈顶指针       */
    int MaxSize;        /* 堆栈最大容量   */
};
typedef PtrToSNode Stack;

Stack CreateStack(int MaxSize)
{
    Stack S = (Stack)malloc(sizeof(struct SNode));
    S->Data = (ElementType*)malloc(MaxSize * sizeof(ElementType));
    S->Top = 0;
    S->MaxSize = MaxSize;
    return S;
}

bool Push(Stack S, ElementType X);
ElementType Pop(Stack S);

Operation GetOp();          /* 裁判实现，细节不表 */
void PrintStack(Stack S); /* 裁判实现，细节不表 */

int main()
{
    ElementType X;
    Stack S;
    int N, done = 0;

    scanf("%d", &N);
    S = CreateStack(N);
    while (!done) {
        switch (GetOp()) {
        case push:
            scanf("%d", &X);
            Push(S, X);
            break;
        case pop:
            X = Pop(S);
            if (X != ERROR) printf("%d is out\n", X);
            break;
        case end:
            PrintStack(S);
            done = 1;
            break;
        }
    }
    return 0;
}



bool Push(Stack S, ElementType X) {
    if (S->MaxSize == S->Top) {
        printf("Stack Full\n");
        return false;
    }
    S->Data[S->Top] = X;
    S->Top++;
    return true;
}

ElementType Pop(Stack S) {
    if (S->Top == 0) {
        printf("Stack Empty\n");
        return false;
    }
    else {
        S->Top--;
        return S->Data[S->Top];
    }
}