#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int main()
//{
//	int a = 0;
//	int* pa = &a; //pa为整型指针 —— 存放整型的指针
//
//	char b = 0;
//	char* pb = &b; //pb为字符指针 —— 存放字符的指针
//
//	int arr[10] = { 0 };
//	int(*parr)[10] = &arr;  //parr为数组指针 —— 存放数组的指针
//
//	return 0;
//}

//#include<stdio.h>
//
//void Print1(int parr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", parr[i]);
//	}
//}
//
//void Print2(int(*parr)[5], int sz)
//{
//	int j = 0;
//	for (j = 0; j < 5; j++)
//	{
//		printf("%d ", parr[0][j]);
//	}
//}
//
//int main()
//{
//	int arr[5] = { 1, 2, 3, 4, 5 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Print1(arr, sz);
//	printf("\n");
//	Print2(&arr, sz);
//	return 0;
//}



//#include<stdio.h>
//
//void Print1(int arr[3][3], int x, int y)
//{
//	int i = 0;
//	for (i = 0; i < x; i++)
//	{
//		int j = 0;
//		for (j = 0; j < y; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//void Print2(int(*parr)[3], int x, int y)
//{
//	int i = 0;
//	for (i = 0; i < x; i++)
//	{
//		int j = 0;
//		for (j = 0; j < y; j++)
//		{
//			printf("%d ", parr[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][3] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
//	Print1(arr, 3, 3);
//	Print2(arr, 3, 3);
//	return 0;
//}

