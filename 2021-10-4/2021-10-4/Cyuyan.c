#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int main()
//{
//	//第一种用法
//	char ch1 = 'q';
//	char* pc = &ch1;
//
//	//第二种用法
//	char* ch2 = "abcdef";
//	printf("%c\n", *ch2);
//	printf("%s\n", ch2);
//
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	//存放整数地址的数组
//	int a = 10;
//	int b = 20;
//	int c = 30;
//	int d = 40;
//	int* arr[4] = { &a, &b, &c, &d };
//	//利用指针数组打印a, b, c, d的值
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		printf("%d ", *arr[i]);
//	}
//	return 0;
//}


//#include<stdio.h>
//
//int main()
//{
//	int arr1[5] = { 1, 2, 3, 4, 5 };
//	int arr2[5] = { 2, 3, 4, 5, 6 };
//	int arr3[5] = { 3, 4, 5, 6, 7 };
//	//存放整型数组首元素地址的数组
//	int* arr[3] = { arr1, arr2, arr3 };
//	//利用存放整型数组首元素地址的数组打印数组中的数
//	int i = 1;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	char* ch1 = "abc";
//	char* ch2 = "def";
//	char* ch3 = "ghi";
//	//存放常量字符串首元素地址的数组
//	char* ch[3] = { ch1, ch2, ch3 };
//	//利用存放常量字符串首元素地址的数组，打印常量字符串
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%s\n", ch[i]);
//	}
//	return 0;
//}