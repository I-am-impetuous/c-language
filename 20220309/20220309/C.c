#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int factorsum(int number);
//void PrintPN(int m, int n);
//
//int main()
//{
//    int m, n;
//
//    scanf("%d %d", &m, &n);
//    if (factorsum(m) == m) printf("%d is a perfect number\n", m);
//    if (factorsum(n) == n) printf("%d is a perfect number\n", n);
//    PrintPN(m, n);
//
//    return 0;
//}
//
//int factorsum(int number) {
//    int i = 0;
//    int sum = 0;
//    for (i = 1; i < number; i++) {
//        if (number % i == 0) {
//            sum += i;
//        }
//    }
//    return sum;
//}
//
//void PrintPN(int m, int n) {
//    int j = 0;
//    int count = 0;
//    for (j = m; j <= n; j++) {
//        if (factorsum(j) == j) {
//            count++;
//            printf("%d = 1 ", j);
//            int i = 0;
//            for (i = 2; i < j; i++) {
//                if (j % i == 0) {
//                    printf("+ %d ", i);
//                }
//            }
//            printf("\n");
//        }
//    }
//    if (count == 0) {
//        printf("No perfect number");
//    }
//}






//#include <stdio.h>
//
//int fib(int n);
//void PrintFN(int m, int n);
//
//int main()
//{
//    int m, n, t;
//
//    scanf("%d %d %d", &m, &n, &t);
//    printf("fib(%d) = %d\n", t, fib(t));
//    PrintFN(m, n);
//
//    return 0;
//}
//
//
//
//int fib(int n) {
//    if (n == 0) {
//        return -1;
//    }
//    int i = 0;
//    int num1 = 0;
//    int num2 = 1;
//    int sum = 1;
//    for (i = 0; i < n - 1; i++) {
//        sum = num1 + num2;
//        num1 = num2;
//        num2 = sum;
//    }
//    return sum;
//}
//
//void PrintFN(int m, int n) {
//    int i = 0;
//    int flag = 0;
//    for (i = m; i <= n; i++) {
//        int j = 0;
//        for (j = 0; j <= i; j++) {
//            if (fib(j) == i) {
//                flag = 1;
//                printf("%d ", i);
//                break;
//            }
//        }
//    }
//    if (flag == 0) {
//        printf("No Fibonacci number");
//    }
//}


//#include <stdio.h>
//
//int reverse(int number);
//
//int main()
//{
//    int n;
//
//    scanf("%d", &n);
//    printf("%d\n", reverse(n));
//
//    return 0;
//}
//
//int reverse(int number) {
//    if (number == 0) {
//        return 0;
//    }
//    int temp = 0;
//    int flag = 0;
//    int count = 0;
//    int sum = 0;
//    if (number < 0) {
//        flag = 1;
//        number = -number;
//    }
//    temp = number;
//    while (temp > 0) {
//        temp /= 10;
//        count++;
//    }
//    while (number > 0) {
//        int num = number % 10;
//        int i = 0;
//        for (i = count - 1; i > 0; i--) {
//            num *= 10;
//        }
//        count--;
//        sum += num;
//        number /= 10;
//    }
//
//    if (flag == 1) {
//        sum = -sum;
//    }
//    return sum;
//}


//#include <stdio.h>
//#include <string.h>
//
//#define MAXS 10
//
//void Shift(char s[]);
//
//void GetString(char s[]); /* 实现细节在此不表 */
//
//int main()
//{
//    char s[MAXS];
//
//    GetString(s);
//    Shift(s);
//    printf("%s\n", s);
//
//    return 0;
//}
//
//void Shift(char s[]) {
//    int i = 0;
//    int length = 0;
//    while (s[i] != '\0') {
//        length++;
//        i++;
//    }
//    for (i = 0; i < 3; i++) {
//        char ch = s[0];
//        int j = 0;
//        for (j = 0; j < length - 1; j++) {
//            s[j] = s[j + 1];
//        }
//        s[length - 1] = s[0];
//    }
//}

#include <stdio.h>

void splitfloat(float x, int* intpart, float* fracpart);

int main()
{
    float x, fracpart;
    int intpart;

    scanf("%f", &x);
    splitfloat(x, &intpart, &fracpart);
    printf("The integer part is %d\n", intpart);
    printf("The fractional part is %g\n", fracpart);

    return 0;
}

void splitfloat(float x, int* intpart, float* fracpart) {
    *intpart = (int)x;
    *fracpart = x - (float)*intpart;
}