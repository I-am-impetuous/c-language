#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//
//int main()
//{
//	int a[3][4] = { 0 };
//	printf("1.！！ %d\n", sizeof(a));
//	printf("2.！！ %d\n", sizeof(a[0][0]));
//	printf("3.！！ %d\n", sizeof(a[0]));
//	printf("4.！！ %d\n", sizeof(a[0] + 1));
//	printf("5.！！ %d\n", sizeof(*(a[0] + 1)));
//	printf("6.！！ %d\n", sizeof(a + 1));
//	printf("7.！！ %d\n", sizeof(*(a + 1)));
//	printf("8.！！ %d\n", sizeof(&a[0] + 1));
//	printf("9.！！ %d\n", sizeof(*(&a[0] + 1)));
//	printf("10.！！ %d\n", sizeof(*a));
//	printf("11.！！ %d\n", sizeof(a[3]));
//	return 0;
//}

int main()
{
	char* c[] = { "ENTER","NEW","POINT","FIRST" };
	char** cp[] = { c + 3,c + 2,c + 1,c };
	char*** cpp = cp;
	printf("%s\n", **++cpp);
	printf("%s\n", *-- * ++cpp + 3);
	printf("%s\n", *cpp[-2] + 3);
	printf("%s\n", cpp[-1][-1] + 1);
	return 0;
}