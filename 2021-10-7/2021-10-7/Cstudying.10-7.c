#define _CRT_SECURE_NO_WARNINGS 1

//void —— 无具体指针类型
//能够接收任意类型的地址
//缺点不能进行运算，不能加减整数，不能解引用


//------------------------------------------------------------------------------
////整型的冒泡排序
//#include<stdio.h>
//
//void bubble_sort(int arr[], int sz)
//{
//	int i = 0;
//	//跑多少趟
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		//比较的对数
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			//交换
//			if (arr[j] > arr[j + 1])
//			{
//				int tem = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tem;
//			}
//		}
//	}
//}
////打印冒泡排序之后的值
//void Print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//int main()
//{
//	int arr[10] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//冒泡排序
//	bubble_sort(arr, sz);
//	//打印排序之后的函数
//	Print(arr, sz);
//	return 0;
//}

//-------------------------------------------------------------------------
//void qsort(void* base,
//	size_t num, 
//	size_t width, 
//	int(__cdecl* compare)(const void* elem1, const void* elem2));


////-----------------------------------------------------------------------
////排序整型
//#include<stdio.h>
//#include<stdlib.h>
// 
//
////打印
//void Print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//
//int cmp_int(const void* e1, const void* e2)
//{
//	return *(int*)e1 - *(int*)e2;
//}
//
//int main()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	//排序
//	qsort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), cmp_int);
//	//打印
//	Print(arr, sizeof(arr) / sizeof(arr[0]));
//}


//----------------------------------------------------------------------------------
////排序结构体 —— qsort的使用
//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//
////结构体函数
//struct people
//{
//	char number[20];
//	int age;
//};
//
////qsort中的比较函数 —— 年龄比较
//int cmp_by_age(const void* e1, const void* e2)
//{
//	return ((struct people*)e1)->age - ((struct people*)e2)->age;
//}
//
////打印
//void Print1(struct people* p1, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%10s %4d\n", (p1+i)->number, (p1+i)->age  );
//	}
//}
//
////qsort中的比较函数 —— 名字比较
//int cmp_by_numble(const void* e1, const void* e2)
//{
//	return strcmp(((struct people*)e1)->number , ((struct people*)e2)->number);
//}
//
//
//int main()
//{
//	struct people p1[3] = { {"zhangsan", 30}, {"lisi", 20}, {"wangwu", 15} };
//	int sz = sizeof(p1) / sizeof(p1[0]);
//	qsort(p1, sz, sizeof(struct people), cmp_by_age);
//	Print1(p1, sz);
//	printf("=================================\n");
//	qsort(p1, sz, sizeof(struct people), cmp_by_numble);
//	Print1(p1, sz);
//	return 0;
//}


//---------------------------------------------------------------------------------
////实现一个万能的冒泡排序
//#include<stdio.h>
//
////交换
//void swap_num(char* bulf1, char* bulf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tem = *(bulf1+i);
//		*(bulf1+i) = *(bulf2+i);
//		*(bulf2+i) = tem;
//	}
//}
//
////万能的冒泡排序
//void bubble_sort(void* p1, size_t count, size_t width, int(*cmp)(const void* e1, const void* e2))
//{
//	int i = 0;
//	//趟数
//	for (i = 0; i < count - 1; i++)
//	{
//		int j = 0;
//		//每趟的对数
//		for (j = 0; j < count - i - 1; j++)
//		{
//			if(cmp((char*)p1 + j * width, (char*)p1 + (j + 1) * width) > 0)
//				swap_num((char*)p1 + j*width, (char*)p1 + (j+1)*width, width);
//		}
//	}
//}
//
//int main()
//{
//	;
//	return 0;
//}

#include<stdio.h>

int main()
{
	int a[] = { 1,2,3,4 };
	printf("1 .—— %d\n", sizeof(a));        // —————————— 1
	printf("2 .—— %d\n", sizeof(a + 0));      // —————————— 2
	printf("3 .—— %d\n", sizeof(*a));       // —————————— 3
	printf("4 .—— %d\n", sizeof(a + 1));      // —————————— 4
	printf("5 .—— %d\n", sizeof(a[1]));     // —————————— 5
	printf("6 .—— %d\n", sizeof(&a));       // —————————— 6
	printf("7 .—— %d\n", sizeof(*&a));      // —————————— 7
	printf("8 .—— %d\n", sizeof(&a + 1));     // —————————— 8
	printf("9 .—— %d\n", sizeof(&a[0]));    // —————————— 9
	printf("10.—— %d\n", sizeof(&a[0] + 1)); // —————————— 10
	return 0;
}
