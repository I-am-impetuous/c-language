#define _CRT_SECURE_NO_WARNINGS 1

//求两个数最大公约数
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int a = 0;
//	int b = 0;
//	int num = 0;
//	scanf("%d %d", &a, &b);
//	for (i = 1; i <= a; i++)
//	{
//		if (a % i == 0 && b % i == 0)
//		{
//			num = i;
//		}
//	}
//	printf("%d\n", num);
//	return 0;
//}


//从1000年到2000年输入其中的闰年。
// 闰年每四年为一个闰年。如果是世纪闰年必须是400的倍数。
//#include<stdio.h>
//int main()
//{
//	int num1 = 0;
//	int i = 0;
//	for (i = 1000; i <= 2000; i++)
//	{
//		if (i % 4 == 0 && i % 100 != 0)
//			printf("%d ", i);
//		if (i % 400 == 0)
//			printf("%d ", i);
//	}
//	return 0;
//
//}


//求100到200其中之间的素数，并打印出来。
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int n = 0;
//	int item = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		item = 0;
//		for (n = 1; n <= i; n++)
//		{
//			if (i % n == 0)
//			{
//				item++;
//			}	
//		}
//		if (item == 2)
//			printf("%d ", i);
//	}
//	return 0;
//}
//方法4
/*
继续对方法三优化，只要i不被[2, sqrt(i)]之间的任何数据整除，则i是素数，但是实际在操作时i不用从101逐渐递增到200，因为出了2和3之外，不会有两个连续相邻的数据同时为素数
*/
//int main()
//{
//	int i = 0;
//	int count = 0;
//
//
//	for (i = 101; i <= 200; i += 2)
//	{
//		//判断i是否为素数
//		//2->i-1
//		int j = 0;
//		for (j = 2; j <= sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				break;
//			}
//		}
//		//...
//		if (j > sqrt(i))
//		{
//			count++;
//			printf("%d ", i);
//		}
//	}
//
//	printf("\ncount = %d\n", count);
//	return 0;
//}


//求1到100之间9出现的次数。
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 10 == 9)
//			count++;
//		if (i / 10 == 9)
//			count++;
//	}
//	printf("9出现的个数 = %d\n", count);
//	return 0;
//}


//求一个公式相加的和
//#include<stdio.h>
//int main()
//{
//	float i = 0.0;
//	float sum1 = 0.0;
//	float sum2 = 0.0;
//	for (i = 1; i < 100; i += 2)
//	{
//		sum1 += 1 / i;
//	}
//	for (i = 2; i <= 100; i += 2)
//	{
//		sum2 += (-1) / i;
//	}
//	printf("%f\n", sum1 + sum2);
//	return 0;
//}


//打印10个数并求这10个数其中最大的值。
//#include<stdio.h>
//
////int MAX(int x, int y)
//{
//	return(x > y ?  x : y);
//}
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	int i = 0;
//	scanf("%d", &num1);
//	for (i = 1; i < 10; i++)
//	{
//		scanf("%d", &num2);
//		num1 = MAX(num1, num2);
//	}
//	printf("%d\n", num1);
//	return 0;
//}


//打印出来九九乘法表。
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int n = 0;
//	for (i = 1; i <= 10; i++)
//	{
//		for (n = 1; n <= 10; n++)
//		{
//			if (i >= n)
//				printf("%d*%d=%d  ", i, n, i * n);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//折半查找法，二分法。从1到10的数字中查找他们所对应的下标。
#include<stdio.h>
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int left = 0;
	int right = sizeof(arr) / sizeof(arr[0]) - 1;
	int mid = 0;
	int num = 0;
	scanf("%d", &num);
	while (left <= right)
	{
		mid = (left + right) / 2;
		if (num > arr[mid])
		{
			left = mid + 1;
		}
		if (num < arr[mid])
		{
			right = mid - 1;
		}
		if (num == arr[mid])
		{
			printf("找到了，下标为：%d\n", mid);
			break;
		}
	}
	if (left > right)
		printf("输入错误，没有找到\n");
	return 0;
}


