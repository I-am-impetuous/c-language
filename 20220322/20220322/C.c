#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int fac(int n) {
//    if (n == 1) {
//        return 1;
//    }
//    if (n == 2) {
//        return 1;
//    }
//    return fac(n - 1) + fac(n - 2);
//}
//
//int main() {
//    int n, sum = 1, month = 1;
//    scanf("%d", &n);
//    while (sum < n) {
//        sum = fac(month);
//        month++;
//    }
//    printf("%d", month);
//    return 0;
//}


#define MAXSIZE 100
#include <stdio.h>
#include <malloc.h>
typedef int DataType;
typedef struct node {
    DataType data[MAXSIZE];
    int length;
}SeqList, * PSeqList;

//函数申明区
PSeqList Init_SeqList();//创建一个顺序表
int Destroy_SeqList(PSeqList* PL);//销毁顺序表
int Input_SeqList(PSeqList PL);//将数据输入到顺序表中
int Output_SeqList(PSeqList PL);//输出顺序表
void pur_SeqList(PSeqList PL);//您要实现的函数

int main()
{
    PSeqList PL;
    PL = Init_SeqList();
    Input_SeqList(PL);

    pur_SeqList(PL);
    Output_SeqList(PL);
    Destroy_SeqList(&PL);

    return 0;
}

//创建一个顺序表，入口参数无，返回一个指向顺序表的指针，指针值为零表示分配空间失败
PSeqList Init_SeqList()
{
    PSeqList PL;
    PL = (PSeqList)malloc(sizeof(SeqList));
    if (PL)
        PL->length = 0;

    return (PL);
}

//销毁顺序表，入口参数：为要销毁的顺序表指针地址 
int  Destroy_SeqList(PSeqList* PL)
{
    if (*PL)
        free(*PL);
    *PL = NULL;

    return  1;
}
//将输入数据保存到顺序表中 
int Input_SeqList(PSeqList L)
{
    int i = 0, x;
    scanf("%d", &x);
    while (x >= 0) {
        L->data[i] = x;
        i++;
        scanf("%d", &x);
    }
    L->length = i;
    return 1;
}
//顺序表整体输出
int Output_SeqList(PSeqList PL)
{
    int i;
    for (i = 0; i < PL->length; i++)
        printf("%d ", PL->data[i]);

}


void pur_SeqList(PSeqList PL) {
    int i = 0, num = 0, cur = 0;
    for (i = 0; i < PL->length - 1; i++ ) {
        num = PL->data[i];
        for (int k = i; k < PL->length - 1;) {
            cur = PL->data[k + 1];
            if (cur == num) {
                //删除顺序表的元素
                int j = 0;
                for (j = k + 1; j < PL->length - 1; j++) {
                    PL->data[j] = PL->data[j + 1];
                }
                PL->length--;
            }
            else {
                k++; 
            }
        }
    }
}


//void pur_SeqList(PSeqList PL) {
//    int i, a, b, count = 0;
//    for (i = 0; i < PL->length; i++) {
//        int j = i + 1;
//        a = PL->data[i];
//        b = PL->data[j];
//        count = 0;
//        for (j = i + 1; j < PL->length; j++) {
//            b = PL->data[j];
//            if (a == b) {
//                count++;
//            }
//            else {
//                PL->data[j - count] = PL->data[j];
//            }
//        }
//        PL->length = PL->length - count;
//    }
//}