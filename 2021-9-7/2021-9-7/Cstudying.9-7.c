#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	a = -a;
//	printf("%d\n", a);   //打印值为-10
//	a = -a;
//	printf("%d\n", a);   //打印值为 10
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//    int a = 10;
//    int arr[10] = { 0 };
//    int* p = &a;  // *表示 p 是指针变量，int 表示p地址所指向的内容是整型
//                  // & —— 表示取出a的地址  p = &a —— 表示把a的地址放在P指针中
//    *p = 20;   //*是解引用操作符， 能从地址找到地址中的内容
//    //int* p 和 *p = 20 中的*p是不一样的
//    //其中的int* p中的 * 表示p是一个指针变量
//    //*p = 20 中的 * 表示解引用，利用p中存放的地址找到地址中的内容
//    &arr;//—— 表示取出整个数组的地址 —— &arr + 1 跳过40个字节（10个整型）
//    arr;//—— 表示取出首元素的地址 —— arr + 1 跳过4个字节（一个整型）
//    &arr[0];// —— 表示取出首元素的地址 —— &arr[0] + 1 —— 跳过4个字节（一个整型）
//}

//#include<stdio.h>
//int main()
//{
//    char ch[10] = "abcd";
//    int a = 10;
//    printf("%d\n", sizeof(ch)); //打印值为10
//    printf("%d\n", strlen(ch));  //打印值为4 —— strlen是计算字符串的大小，遇见\0停止
//    printf("%d\n", sizeof(int)); //打印值为4
//    printf("%d\n", sizeof(a)); //打印值为4
//    printf("%d\n", sizeof a); //打印值为4 —— 从这个就可以说明sizeof是操作符，不是函数
//                              //如果是函数就必须有函数引用操作符 —— ()
//    printf("%d\n", sizeof int); //这种写法是错误的
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 5;
//	short b = 10;
//	printf("%d\n", sizeof(b = a + 2));
//	printf("%d\n", b);
//}