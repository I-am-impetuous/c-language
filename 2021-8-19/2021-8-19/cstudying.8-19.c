#define _CRT_SECURE_NO_WARNINGS 1


//#include<stdio.h>
//void swap2(int* px, int* py)
//{
//	int tem = *px;
//	*px = *py;
//	*py = tem;
//}
//void swap1(int x, int y)
//{
//	int tem = x;
//	x = y;
//	y = tem;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("交换前：%d %d", a, b);
//	swap1(a, b);
//	swap2(&a, &b);
//	printf("交换后：%d %d", a, b);
//	return 0;
//}

//is_prime    is_leap_year    binary_search     


//写一个函数可以判断一个数是不是素数。
//#include<stdio.h>
//#include<math.h>
//
//int is_prime(int i)
//{
//	int j = 0;
//	for (j = 2; j < sqrt(i); j++)
//	{
//		if (i % j == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//
//int main()
//{
//	int i = 0;
//	//判断100到200之间的素数
//	for (i = 100; i < 200; i++)
//	{
//		//规定如果是素数：返回值为1、不是素数返回值为0。
//		int ret = is_prime(i);
//		if (ret == 1)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


//#include<stdio.h>
//
////函数:判断不是闰年。
//int is_leap_year(int i)
//{
//	if (((i % 4 == 0) && (i % 100 != 0)) || (i % 400 == 0))
//		return 1;
//	return 0;
//}
//
//int main()
//{
//	int i = 0;
//	//判断1000到2000之间的年份之间的闰年
//	for (i = 1000; i <= 2000; i++)
//	{
//
//		//规定如果是闰年：返回1、否则返回0。
//		int ret = is_leap_year(i);
//		if (ret == 1)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


////写一个函数，实现一个整形有序数组的二分查找。
//#include<stdio.h>
////二分查找数字
//int binary_search(int arr[10], int sz, int n)
//{
//	int left = 0;
//	int right = sz - 1;
//	int mid = 0;
//	while (left <= right)
//	{
//		mid = (left + right) / 2;
//		if (arr[mid] < n)
//		{
//			left = mid + 1;
//		}
//		else if(arr[mid] > n)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	return -1;
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]); //计算数组的元素个数
//	int n = 0;
//	scanf("%d", &n);
//	//规定：找到了返回下标，没有找到返回-1
//	int ret = binary_search(arr, sz, n);
//	if (ret == -1)
//	{
//		printf("没有找到\n");
//	}
//	else
//	{
//		printf("找到了：下标为：%d\n", ret);
//	}
//	return 0;
//}


//#include<stdio.h>
//
//int Print(int num)
//{
//	return ++num;
//}
//
//int main()
//{
//	int num = 0;
//	num = Print(num);
//	printf("%d\n", num);  //1
//	num = Print(num);
//	printf("%d\n", num);  //2
//	num = Print(num);
//	printf("%d\n", num);  //3
//}


//#include<stdio.h>
//
//void two()
//{
//	printf("hello\n");
//}
//
//void one()
//{
//	two();
//	printf("haha\n");
//}
//int main()
//{
//	one();
//	printf("hehe\n");
//	return 0;
//}

//#include<stdio.h>


//int main()
//{
//	void two()
//	{
//		printf("hello\n");
//	}
//
//	void one()
//	{
//		two();
//		printf("haha\n");
//	}
//	printf("hehe\n");
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", sizeof(arr));
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[10] = "xxxxxxx";
//	char arr2[10] = "hehe";
//	printf("%s\n", strcpy(arr1, arr2));
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	printf("%d", printf("%d", printf("43")));
//	return 0;
//}

//00010011
//00011111
//00010011
//00010011
//00011111
//00011111


//要求写一个函数，将字符串中的空格替换为%20。样例： “abc defgx yz” 转换成 “abc%20defgx%20yz”
//#include<stdio.h>
//int main()
//{
//	char ch = 0;
//	while ((ch = getchar()) != EOF)
//	{
//		if (ch == ' ')
//		{
//			printf("%%20");
//			continue;
//		}
//		putchar(ch);
//	}
//	return 0;
//}