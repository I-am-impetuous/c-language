#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

template<class T>
T arrayMin(T *array, int size) {
	T min = array[0];
	for (int i = 1; i < size; i++) {
		if (min > array[i]) {
			min = array[i];
		}
	}
	return min;
}


int main() {
	int array1[5] = { 1,2,3,4,-1 };
	double array2[5] = { 1.1,4.5,8.9,1.9,2.0 };

	cout << arrayMin(array1, 5) << endl;
	cout << arrayMin(array2, 5) << endl;
}