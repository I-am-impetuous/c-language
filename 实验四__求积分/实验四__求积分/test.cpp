#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<string>
using namespace std;

double f(double x);
double getResult(double a, double b);

int main()
{
	double a = 0;
	double b = 2;
	double result = getResult(a, b);
	cout << "3 * x^2 在 " << a << " 到 " << b << " 上的定积分为：" << result << endl;
	system("pause");
}

double f(double x)
{
	return 3 * x * x;
}

double getResult(double a, double b)
{
	double result = (b - a) * (f(a) + 4 * f((a + b) / 2) + f(b)) / 6;
	return result;
}