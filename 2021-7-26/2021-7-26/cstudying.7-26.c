#define _CRT_SECURE_NO_WARNINGS 1

//递归和非递归分别实现strlen
//#include<stdio.h>
//
//int my_strlen(char* s)
//{
//	int count = 0;
//	while(*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[20] = "abcdef";
//	int num = my_strlen(arr);
//	printf("%d\n", num);
//	return 0;
//}


//#include<stdio.h>
//
//int my_strlen(char* s)
//{
//	if (*s != '\0')
//		return my_strlen(s + 1) + 1;
//	else
//		return 0;
//}
//
//int main()
//{
//	char arr[20] = "abcdef";
//	int num = my_strlen(arr);
//	printf("%d\n", num);
//	return 0;
//}


////把一个字符反向排序（利用递归）
//#include<stdio.h>
//
//int my_strlen(char* string1)
//{
//	int count = 0;
//	while (*string1 != '\0')
//	{
//		string1++;
//		count++;
//	}
//	return count;
//}
//
//void reverse_string(char* string)
//{
//	int left = 0;
//	int right = 0;
//	right = my_strlen(string);
//	while (left < right)
//	{
//		char swap = 0; 
//		right--;
//		swap = string[left];
//		string[left] = string[right];
//		string[right] = swap;
//		left++;
//	}
//}
//
//int main()
//{
//	char  arr1[20] = "abcdef";
//	reverse_string(arr1);
//	printf("%s\n", arr1);
//	return 0;
//}


//把一个字符反向排序（利用递归）
//#include<stdio.h>
//
//int my_strlen(char* string1)
//{
//	int count = 0;
//	while (*string1 != '\0')
//	{
//		string1++;
//		count++;
//	}
//	return count;
//}
//
//void reverse_string(char* string)
//{
//	int right = my_strlen(string) - 1;
//	char swap_ = 0;
//	swap_ = string[0];
//	string[0] = string[right];
//	string[right] = '\0';
//	if (right - 2 >= 2)
//		reverse_string(string + 1);
//	string[right] = swap_;
//}
//
//int main()
//{
//	char  arr1[20] = "abcdef";
//	reverse_string(arr1);
//	printf("%s\n", arr1);
//	return 0;
//}


////写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//#include<stdio.h>
//
//int digit_sum(int x)
//{
//	if (x > 9)
//	{
//		return digit_sum(x / 10) + x % 10;
//	}
//	else
//	{
//		return x;
//	}
//}
//
//int main()
//{
//	int n = 0;
//	int sum = 0;
//	scanf("%d", &n);
//	sum = digit_sum(n);
//	printf("%d\n", sum);
//	return 0;
//}

////编写一个函数实现n的k次方，使用递归实现。
//#include<stdio.h>
//int power(int n)
//{
//	if (n > 0)
//	{
//		return power(n - 1) * n;
//	}
//	else if (n == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return 1.0 / power(n + 1) * n;
//	}
//}
//int main()
//{
//	int n = 0;
//	float sum = 0;
//	scanf("%d", &n);
//	sum = power(n);
//	printf("%f\n", sum);
//	return 0;
//}


////递归实现求第n个斐波那契数
//#include<stdio.h>
//
//int fibonacci(int n)
//{
//	if (n == 1 || n == 2)
//	{
//		return 1;
//	}
//	else
//	{
//		return fibonacci(n - 1) + fibonacci(n - 2);
//	}
//}
//
//int main()
//{
//	int sum = 0;
//	int n = 0;
//	scanf("%d", &n);
//	sum = fibonacci(n);
//	printf("%d\n", sum);
//	return 0;
//}


//用非递归实现斐波那契数
//#include<stdio.h>
//
//int fibonacci(int n)
//{
//	int x = 1;
//	int y = 1;
//	int i = 1;
//	int sum = 0;
//	for (i = 2; i < n; i++)
//	{
//		sum = x + y;
//		x = y;
//		y = sum;
//	}
//	return sum;
//}
//int main()
//{
//	int sum = 0;
//	int n = 0;
//	scanf("%d", &n);
//	sum = fibonacci(n);
//	printf("%d", sum);
//	return 0;
//}

//小乐乐在课上学习了二进制八进制与十六进制后
//，对进制转换产生了浓厚的兴趣。因为他的幸运数
//字是6，所以他想知道一个数表示为六进制后的结果
//。请你帮助他解决这个问题。
//#include<stdio.h>
//
//void swap(int n)
//{
//    if (n > 0)
//    {
//        swap(n / 6);
//        printf("%d", n % 6);
//    }
//}
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    swap(n);
//    return 0;
//}


//小乐乐学校教学楼的电梯前排了很多人，他的前面有n个人
//在等电梯。电梯每次可以乘坐12人，每次上下需要的时间为4
//分钟（上需要2分钟，下需要2分钟）。请帮助小乐乐计算还
//需要多少分钟才能乘电梯到达楼上。（假设最初电梯在1层）
//#include<stdio.h>
//int waiting(int n)
//{
//    int sum = 2;
//    while (n >= 12)
//    {
//        sum += 4;
//        n -= 12;
//    }
//    return sum;
//}
//int main()
//{
//    int n = 0;
//    int sum = 2;
//    scanf("%d", &n);
//    sum = waiting(n);
//    printf("%d\n", sum);
//    return 0;
//}

//#include<stdio.h>
//
//long long int max_div(long long int n, long long int m)
//{
//    long long int item = 1;
//    while (item != 0)
//    {
//        item = m % n;
//        m = n;
//        n = item;
//    }
//    return m;
//}
//
//long long int min_mul(long long int n, long long int m)
//{
//    long long int item = 1;
//    long long int sum3 = 0;
//    sum3 = n;
//    while (item != 0)
//    {
//        sum3 += n;
//        item = sum3 % m;
//    }
//    return sum3;
//}
//
//int main()
//{
//    long long int n = 0;
//    long long int m = 0;
//    long long int sum1 = 0;
//    long long int sum2 = 0;
//    scanf("%d", &n);
//    scanf("%d", &m);
//    sum1 = max_div(n, m);
//    sum2 = min_mul(n, m);
//    printf("%d\n", sum1 + sum2);
//    return 0;
//}


//#include <stdio.h>
//int main()
//{
//    int ch = 0;
//    while ((ch = getchar()) != EOF)
//    {
//        if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
//        {
//            printf("YES\n");
//        }
//        else
//        {
//            printf("NO\n");
//        }
//        getchar();
//    }
//    return 0;



//打印空心正方形；
//#include<stdio.h>
//int main()
//{
//    int num = 0;
//    int i = 0;
//    int n = 0;
//    scanf("%d", &num);
//    for (i = 0; i < num; i++)
//    {
//        if (i == 0 || i == num - 1)
//        {
//            for (n = 0; n < num; n++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//        else
//        {
//            for (n = 0; n < num; n++)
//            {
//                if (n == 0 || n == num - 1)
//                    printf("* ");
//                else
//                    printf("  ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}


//公务员面试现场打分。有7位考官，从键盘
// 输入若干组成绩，每组7个分数（百分制）
// ，去掉一个最高分和一个最低分，输出每
// 组的平均成绩。
// 
//#include<stdio.h>
//
//int main()
//{
//    int i = 0;
//    int max = 0;
//    int min = 100;
//    int num = 0;
//    float sum = 0;
//    for (i = 0; i < 7; i++)
//    {
//        scanf("%d", &num);
//        sum += num;
//        if (min > num)
//            min = num;
//        if (max < num)
//            max = num;
//    }
//    printf("%.2f\n", (sum - max - min) / 5.0);
//    return 0;
//}


//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    scanf("%d %d %d", &a, &b, &c);
//    if (a + b > c && b + c > a && a + c > b && a - b < c && b - c < a && c - a < b)
//    {
//        if (a == b && b == c && a == c)
//            printf("Equilateral triangle!\n");
//        else if (a == b || b == c || a == c)
//            printf("Isosceles triangle!\n");
//        else
//            printf("Ordinary triangle!");
//    }
//    else
//    {
//        printf("Not a triangle!");
//    }
//    return 0;
//}