#define _CRT_SECURE_NO_WARNINGS 1

//7位评委打分去掉最高分和最低分，然后计算平均值
//#include<stdio.h>
//
//int main()
//{
//    int i = 0;
//    int max = 0;
//    int min = 100;
//    int num = 0;
//    float sum = 0;
//    for (i = 0; i < 7; i++)
//    {
//        scanf("%d", &num);
//        sum += num;
//        if (min > num)
//            min = num;
//        if (max < num)
//            max = num;
//    }
//    printf("%.2f\n", (sum - max - min) / 5.0);
//    return 0;
//}


//创建一个数组，完成对数组的一些操作
//#include<stdio.h>
//
//void init_arr1(int arr1[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr1[i] = 0;
//	}
//}
//
//void Print(int arr1[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//}
//
//void Swap(int arr2[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//		int tmp = 0;
//		tmp = arr2[left];
//		arr2[left] = arr2[right];
//		arr2[right] = tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	int arr1[5] = { 0,1,2,3,4 };
//	int arr2[5] = { 1,2,3,4,5 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	//初始化数组，把数组中的数都放0
//	init_arr1(arr1, sz);
//	//打印数组中的每一个数
//	Print(arr1, sz);
//	//完成数组的逆置
//	Swap(arr2, sz);
//	printf("\n");
//	//打印逆置的函数
//	Print(arr2, sz);
//	return 0;
// }

//计算两个数的不同二进制位的个数
//#include<stdio.h>
//int main()
//{
//    int num1 = 0;
//    int num2 = 0;
//    int count = 0;
//    int num3 = 0;
//    int i = 0;
//    scanf("%d %d", &num1, &num2);
//    num3 = num1 ^ num2;
//    for (i = 0; i < 32; i++)
//    {
//        if ((num3) & 1 == 1)
//            count++;
//        num3 = num3 >> 1;
//    }
//    printf("%d\n", count);
//}


//打印二进制的奇数位和偶数位
//#include<stdio.h>
//
//int main()
//{
//	int i = 0;
//	int num = 0;
//	int x = 0;
//	scanf("%d", &num);
//	x = num;
//	printf("打印奇数位：\n");
//	for (i = 1; i <= 16; i++)
//	{
//		if ((num & 1) == 1)
//			printf("1");
//		else
//			printf("0");
//		 num = num >> 2;
//	}
//	printf("\n");
//
//	printf("打印偶数位：\n");
//	num = x;
//	num = num >> 1;
//	for (i = 1; i <= 16; i++)
//	{
//		if ((num & 1) == 0)
//			printf("0");
//		else
//			printf("1");
//		num = num >> 2;
//	}
//	printf("\n");
//
//	return 0;
//}


//计算一个整数二进制中1的个数
//class Solution
//{
//    public int NumberOf1(int n)
//    {
//        // write code here
//        int flog = 1;
//        int count = 0;
//        while (flog != 0)
//        {
//            if ((n & flog) != 0)
//            {
//                count++;
//            }
//            flog <<= 1;
//        }
//        return count;
//    }
//}

//#include<stdio.h>
//
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	scanf("%d %d", &num1, &num2);
//	printf("交换前：%d %d\n", num1, num2);
//	num1 = num1 ^ num2;
//	num2 = num1 ^ num2;
//	num1 = num1 ^ num2;
//	printf("交换后：%d %d", num1, num2);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//    int ch = 0;
//    printf("请输入字母：");
//    while ((ch = getchar()) != EOF)
//    {
//        if (ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U' || ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
//            printf("Vowel\n");
//        else
//            printf("Consonant\n");
//        getchar();
//    }
//    return 0;
//}

