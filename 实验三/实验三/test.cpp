#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include"rect.h"


int main() {
	Rectangle rect;
	cout << "初始 rect;" << endl;
	rect.Show();
	rect.Assign(100, 200, 300, 400);
	cout << "赋值后 rect;" << endl;
	rect.Show();
	Rectangle rect1(0, 0, 200, 200);
	cout << "初始 rect1;" << endl;
	rect1.Show();
	rect += rect1;
	cout << "与rect1相加后的rect;" << endl;
	rect.Show();
	rect -= rect1;
	cout << "减去rect1后的rect;" << endl;
	rect.Show();
	Rectangle rect2;
	return 0;
}
