#pragma once
using namespace std;

class Rectangle {
	int left, top;
	int right, bottom;

public:
	static int count;

	Rectangle(int l = 0, int t = 0, int r = 0, int b = 0);  //构造函数
	~Rectangle() {
		count--;
		cout << "对象有" << count << "个" << endl;
	};
	void Assign(int l, int t, int r, int b); //修改

	void SetLeft(int t) { left = t; }
	void SetRight(int t) { right = t; }
	void SetTop(int t) { top = t; }
	void SetBottom(int t) { bottom = t; }

	void Show();
	void operator +=(Rectangle&);
	void operator -=(Rectangle&);
	Rectangle operator+(Rectangle&);
	Rectangle operator-(Rectangle&);
};
