#define _CRT_SECURE_NO_WARNINGS 1

//实现猜数字的游戏
//1、输入0 —— 100之间的数字
//2、输入数字
//猜对了就输入：猜对了
//猜错了就输入：猜大了，或者是猜小了
//3、可以多次玩这个游戏。
//
//#include <stdio.h>
//#include <stdlib.h>
//#include <time.h>
//
//void menu()
//{
//	printf("########################\n");
//	printf("##### 0、退出游戏 ######\n");
//	printf("#####  1、玩游戏  ######\n");
//	printf("########################\n");
//}
//
//void game()
//{
//	//生成随机数
//	int ret = rand() % 100 + 1;
//	//猜数字
//	int guess = 0;
//	printf("请输入所猜数字:");
//	while (1)
//	{
//		scanf("%d", &guess);
//		if (guess < ret)
//		{
//			printf("猜小了\n");
//		}
//		else if (guess > ret)
//		{
//			printf("猜大了\n");
//		}
//		else
//		{
//			printf("恭喜你！猜对了\n");
//			break;
//		}
//	}
//}
//
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请输入序号：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("输入错误，请重新输入：\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}

//利用循环实现：打印100到200之间的素数
#include <stdio.h>
#include <math.h>

int main()
{
	int i = 0;
	int n = 0;
	int flog = 1;
	for (i = 101; i <= 200; i++)
	{
		flog = 1;
		for (n = 2; n <= sqrt(i); n++)
		{
			if (i % n == 0)
			{
				flog = 0;
				break;
			}
		}
		if (flog == 1)
		{
			printf("%d ", i);
		}
	}
	return 0;
}
