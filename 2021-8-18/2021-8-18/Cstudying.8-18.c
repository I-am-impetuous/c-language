#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int my_max(int x, int y)
//{
//	return x > y ? x : y;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int c = my_max(a, b);
//	printf("最大值为：%d\n", c);
//	return 0;
//}

////错误示范
//#include<stdio.h>
//
//void swap(int x, int y)
//{
//	int z = x;
//	x = y;
//	y = z;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("交换前：%d %d\n", a, b);
//	swap(a, b);
//	printf("交换后：%d %d\n", a, b);
//	return 0;
//}


#include<stdio.h>

void swap(int* px, int* py)
{
	int z = *px;
	*px = *py;
	*py = z;
}

int main()
{
	int a = 10;
	int b = 20;
	printf("交换前：%d %d\n", a, b);
	swap(&a, &b);
	printf("交换后：%d %d\n", a, b);
	return 0;
}