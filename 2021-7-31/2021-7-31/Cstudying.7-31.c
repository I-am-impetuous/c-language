#define _CRT_SECURE_NO_WARNINGS 1


//不使用数组下标，用函数打印数组内容
//#include<stdio.h>\
//
//void Print(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//}
//
//int main()
//{
//	int arr[5] = { 0,1,2,3,4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Print(arr, sz);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int i = 0;
//	int tmp = 0;
//	int sum = 0;
//	scanf("%d", &a);
//	for (i = 0; i < 5; i++)
//	{
//		tmp = 10 * tmp + a;
//		sum += tmp;
//	}
//	printf("%d\n", sum);
//	return 0;
//}


//求水仙花数并打印
//水仙花数是指一个N位数，其各位数字的N次方之和恰好等于该数本身
//#include<stdio.h>
//
//int wei_shu(int n)
//{
//	int i = 0;
//	int count = 0;
//	while(n != 0)
//	{
//		if (n % 10 != 0)
//			count++;
//		n = n / 10;
//	}
//	return count;
//}
//
//int jie_cheng(int x, int y)
//{
//	int a = 0;
//	int sum1 = 1;
//	for (a = 1; a <=y; a++)
//	{
//		sum1 *= x;
//	}
//	return sum1;
//}
//
//int main()
//{
//	int i = 0;
//	int ws = 0;
//	int sum = 0; 
//	int j = 0;
//	int sum1 = 0;
//	//ws = wei_shu(153);
//	
//	for (i = 0; i <= 100000; i++)
//	{
//		sum = 0;
//		int num2 = i;
//		ws = wei_shu(i);
//		for (j = 0; j < ws; j++)
//		{
//			int num1 = 0;
//			num1 = num2 % 10;
//			sum1 = jie_cheng(num1, ws);
//			sum += sum1;
//			num2 = num2 / 10;
//		}
//		if (i == sum)
//		{
//			printf("%d ", i);
//		}
//	}
//	//printf("%d", ws);
//	return 0;
//}